var correctCards = 0;
var noOfFieldsToGuess = 6;
var meaning = [['I want to go, arent you ready yet', 'All set; the belay is ready', 'Here I come', 'Come ahead'],
['I cant move because of some slack on the rope', 'Give slack'],
['There is a slack on the rope. Take up all loose ropes.','Take up rope'],
['I am in a secure position at the moment and do not need a belay any longer', 'Echoed to insure that there is no misunderstanding.']];

var answers = ['All set; the belay is ready', 'Here I come', 'Come ahead','Give slack','Take up rope','Echoed to insure that there is no misunderstanding.']

function init() {

  // Hide the success message
  $('#successMessage').hide();
  $('#successMessage').css( {
    left: '580px',
    top: '250px',
    width: 0,
    height: 0
  } );

  // Reset the game
  correctCards = 0;
  $('#cardPile').html( '' );
  $('#cardSlots').html( '' );

  // Create the pile of shuffled cards
  var numbers = Array.from(Array(noOfFieldsToGuess).keys());;
  numbers.sort( function() { return Math.random() - .5 } );

  for ( var i=0; i<numbers.length; i++ ) {
    $('<div>' + answers[numbers[i]] + '</div>').data( 'number', numbers[i] ).attr( 'id', 'card'+numbers[i] ).appendTo( '#cardPile' ).draggable( {
      containment: '#content',
      stack: '#cardPile div',
      cursor: 'move',
      revert: true
    } );
  }

  // Create the card slots
  var words = [ 'On Belay', 'Belay On', 'Climbing', 'Slack', 'Up Rope', 'Off Belay' ];
  for ( var i=0; i<words.length; i++ ) {
    $('<div>' + words[i] + '</div>').data( 'number', i ).appendTo( '#cardSlots' ).droppable( {
      accept: '#cardPile div',
      hoverClass: 'hovered',
      drop: handleCardDrop
    } );
  }

}

function handleCardDrop(event, ui) {
  
  //Grab the slot number and card number
  var slotNumber = $(this).data('number');
  var cardNumber = ui.draggable.data('number');
  
  //If the cards was dropped to the correct slot,
  //change the card colour, position it directly
  //on top of the slot and prevent it being dragged again
  if (slotNumber === cardNumber) {
    ui.draggable.addClass('correct');
    ui.draggable.draggable('disable');
    $(this).droppable('disable');
    ui.draggable.position({
      of: $(this), my: 'left top', at: 'left top'
    });
    //This prevents the card from being
    //pulled back to its initial position
    //once it has been dropped
    ui.draggable.draggable('option', 'revert', false);
    correctCards++; //increment keep track correct cards
  }
  
  //If all the cards have been placed correctly then
  //display a message and reset the cards for
  //another go
  if (correctCards === noOfFieldsToGuess) {
    swal("Good job!", "", "success").then(function(value){
        init();
      });
  } 
}

window.addEventListener("load", function(event) {
  $(init);
});