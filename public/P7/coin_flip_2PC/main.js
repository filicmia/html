// TODO:
// - Handle all outcomes of protocol
// - Handle all error on data channels
// - Add visual timers (time_stamping)
// - Disconnect form p2p server after pairing
// - Custom p2p server (ours?)
// - Add setup step of bit commitment scheme (agreeing on hash function, length of r value, public info - general setting)
// | - > Enable for Ana to choose what vector of bits (fixed length < lenght(r)/2) will represent 0 and what 1
// - Be more verboose when player acts maliciously
// - Add more ways to act maliciously
// - Add sending replaying confirmation of received (important) messages
// - Add detailed description of protocol steps and reasoning behind it

peer = null;
inc_paired_data_connection = null;
paired_data_connection = null;
role = null;
coin_side = null;
opposite_coin_side = null;
recived_commitment_string = null;
commitment_string = null;
secret_r = null;
b_r = null;
final_verdict = null;
cheating_reason = null;
nickname = null;
opposite_nickname = null;


function make_connetion_to_cloud() {
    document.getElementById("forgotten_nickname").innerHTML = '';
    nickname = document.getElementById("nickname").value;
    if (nickname == null || nickname.length <= 0) {
        document.getElementById("forgotten_nickname").innerHTML = 'Pozabil si vnesti ime.<br>'
    } else {
        document.getElementById("nickname").disabled = true;

        peer = new Peer(); //secure connetion to cloud P2P server
        peer.on('open', function (id) {
            console.log('A connection to the PeerServer is r/established. Your is ID is:', id);

            //set p2p_id
            document.getElementById("p2p_id_text").innerHTML = 'Tvoj ID je: ';
            document.getElementById("p2p_id").innerHTML = id;
            //change p2p_status
            document.getElementById("p2p_status").innerHTML = 'POVEZAN';
            document.getElementById("p2p_status").setAttribute("style", "color:MediumSeaGreen;");
            //disable make_conn_button
            document.getElementById("make_conn_button").disabled = true;
            document.getElementById("invite_button").disabled = false;
        });

        peer.on('close', function () {
            console.log('Peer is destroyed. All connections (P2P and cloud) are closed.');
            //set p2p_id
            document.getElementById("p2p_id_text").innerHTML = '';
            document.getElementById("p2p_id").innerHTML = '';
            //change p2p_status
            document.getElementById("p2p_status").innerHTML = 'NEPOVEZAN';
            document.getElementById("p2p_status").setAttribute("style", "color:Tomato;");
            //enable make_conn_button
            document.getElementById("make_conn_button").disabled = false;
        });

        peer.on('disconnected', function () {
            console.log('You have been disconeted from could server');
            // TODO try to reconnect peer.reconnect() if needed
            //change p2p_status nepovezan z neaktivnimi/aktivnimi p2p povezavami
            document.getElementById("p2p_status").innerHTML = 'PREKINJENA POVEZAVA';
            document.getElementById("p2p_status").setAttribute("style", "color:#ffa500;");
            //enable make_conn_button
            document.getElementById("make_conn_button").disabled = false;
        });

        peer.on('error', function (err) {
            console.log(err.type);
            if (err.type == 'peer-unavailable') {
                document.getElementById("successful_data_connection_text").innerHTML = 'Vnesen ID <a style="color:Tomato;">ni</a> pravi.';
            }
            // TODO Be more verboose, use err.type
            //set p2p_id
            document.getElementById("p2p_id_text").innerHTML = '';
            document.getElementById("p2p_id").innerHTML = '';
            //change p2p_status
            document.getElementById("p2p_status").innerHTML = 'NEPOVEZAN';
            document.getElementById("p2p_status").setAttribute("style", "color:Tomato;");
            //enable make_conn_button
            document.getElementById("make_conn_button").disabled = false;
        });

        peer.on('connection', function (dataConnection) { //incoming connections 
            console.log('New data connection is established from a remote peer with id:', dataConnection.peer);
            // TODO hadle incommin connections
            dataConnection.on('open', function () {
                console.log('Connection is established with peer', dataConnection.peer, 'and ready to use');
                if (paired_data_connection != null) {
                    dataConnection.close(); // block all incoming dataconnetions after pairing.
                } else {
                    dataConnection.on('data', function (data) {
                        console.log('Received', data);
                        if (paired_data_connection == null) { //listen for incoming invitations
                            if (data['povabilo'] != null) {
                                opposite_nickname = data['povabilo'];
                                inc_paired_data_connection = dataConnection;
                                document.getElementById("inc_inv_id").innerHTML = '(od <b>' + opposite_nickname + '@' + dataConnection.peer + '</b>)';
                                document.getElementById("accept_inv_button").disabled = false;
                            } else {
                                dataConnection.close();
                            }
                        }
                        //TODO hadle incoming data
                    });
                }
            });

            dataConnection.on('close', function () {
                console.log('Connection closed with peer: ', dataConnection.peer);
            });
            dataConnection.on('error', function (err) {
                console.log(err);
            });
        });
    }
}

invite_id = -1;
function invite_to_connect() {
    invite_id = document.getElementById("invite_id").value;
    //check if id is correct
    if (invite_id.length == null || invite_id.trim().length == 0) {
        document.getElementById("successful_data_connection_text").innerHTML = 'Vnesi ID uporabnika.';
    } else if (invite_id.trim() == peer.id) {
        document.getElementById("successful_data_connection_text").innerHTML = '<a style="color:Tomato;">Ne moreš</a> se povezati na samega sebe.';
    } else {
        dataConnection = peer.connect(invite_id.trim(), reliable = true);
        dataConnection.on('open', function () {
            dataConnection.send({ povabilo: nickname });

            dataConnection.on('data', function (data) {
                if (paired_data_connection == null && data['povabilo_sprejeto'] != null) {
                    opposite_nickname = data['povabilo_sprejeto'];
                    document.getElementById("invite_button").disabled = true;
                    paired_data_connection = dataConnection;
                    document.getElementById("successful_data_connection_text").innerHTML = 'Uspešno si se povezal z uporabikom <b style="color:MediumSeaGreen;">' + opposite_nickname + '@' + paired_data_connection.peer + '</b>';
                    prepare_for_phase3();
                }
            });

            document.getElementById("successful_data_connection_text").innerHTML = 'Počakaj, da uporabnik <b>' + dataConnection.peer + '</b> sprejme povabilo.';
        });


        dataConnection.on('close', function () {
            if (paired_data_connection == null) {
                document.getElementById("successful_data_connection_text").innerHTML = 'Uporabnik <b>' + dataConnection.peer + '</b> <a style="color:Tomato;">je zavrnil</a> povabilo.';
            }
        });

        dataConnection.on('error', function () {
            if (paired_data_connection == null) {
                document.getElementById("successful_data_connection_text").innerHTML = 'Povezovanje z uporabnikom <b>' + dataConnection.peer + '</b> je bilo <a style="color:Tomato;">neuspešno</a>.';
            }
        });
    }
}

function accept_to_connect() {

    document.getElementById("invite_button").disabled = true;
    document.getElementById("accept_inv_button").disabled = true;
    inc_paired_data_connection.send({ povabilo_sprejeto: nickname });
    paired_data_connection = inc_paired_data_connection;
    document.getElementById("successful_data_connection_text").innerHTML = 'Uspešno si se povezal z uporabikom <b style="color:MediumSeaGreen;">' + opposite_nickname + '@' + paired_data_connection.peer + '</b>';
    prepare_for_phase3();
}

function prepare_for_phase3() {
    refresh_chat_box();
    document.getElementById("ana_button").disabled = false;
    document.getElementById("bojan_button").disabled = false;
    paired_data_connection.on('data', function (data) {
        if (data == 'ana') {
            role = 'bojan';
            document.getElementById("ana_button").disabled = true;
            document.getElementById("bojan_button").disabled = true;
            document.getElementById("izbira_vloge").innerHTML = 'Uporabnik <b>' + opposite_nickname + '</b> je izbral vlogo <b>Ane</b>, zato boš igral vlogo <b>Bojana</b>.';
            playing_bojan();
        } else if (data == 'bojan') {
            role = 'ana';
            document.getElementById("ana_button").disabled = true;
            document.getElementById("bojan_button").disabled = true;
            document.getElementById("izbira_vloge").innerHTML = 'Uporabnik <b>' + opposite_nickname + '</b> je izbral vlogo <b>Bojana</b>, zato boš igral vogo <b>Ane</b>.';
            playing_ana();
        }
    });
}

function choose_role(name) {
    document.getElementById("ana_button").disabled = true;
    document.getElementById("bojan_button").disabled = true;
    paired_data_connection.send(name);
    role = name;
    if (role == 'ana') {
        document.getElementById("izbira_vloge").innerHTML = 'Igral boš vogo <b>Ane</b>, uporabnik <b>' + opposite_nickname + '</b> pa vlogo <b>Bojana</b>.';
        playing_ana();
    } else if (role == 'bojan') {
        document.getElementById("izbira_vloge").innerHTML = 'Igral boš vogo <b>Bojana</b>, uporabnik <b>' + opposite_nickname + '</b> pa vlogo <b>Ane</b>.';
        playing_bojan();
    }
}

function playing_ana() {
    document.getElementById("ana_waiting_for_commintment").innerHTML = `Počakaj na zaprisego \\(c = \\text{SHA-256}(b_{\\text{B}}||r)\\) od <b>Bojana</b>.<br>`;
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("ana_waiting_for_commintment")]);
    paired_data_connection.on('data', function (data) {
        if (data['c'] != null) {
            recived_commitment_string = data['c'];
            document.getElementById("ana_reciving_commitment_string").innerHTML = '<b>Bojan</b> ti je poslal zapirsego<br>\\(c = \\text{' + recived_commitment_string + '}\\).<br>' +
                '<a href="https://blog.benjojo.co.uk/post/ssh-randomart-how-does-it-work-art">Randomart</a> slika vrednosti \\(c\\) je: <br>' + randomart_of_hash_value(recived_commitment_string) + '.<br>';

            MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("ana_reciving_commitment_string")]);

            // You could test here if commitment string has correct properties/structure
            document.getElementById("ana_choosing_coin_side").innerHTML = `Izberi 
            <button id="choose_coin_side_a_head" onclick="choose_coin_side_a('head')" >moža \\((b_\\text{A} = 0)\\)</button> 
            ali <button id="choose_coin_side_a_tails" onclick="choose_coin_side_a('tails')" >cifro \\((b_\\text{A} = 1)\\)</button> 
            ali prepusti izbiro računalniku <br><button id="choose_coin_side_a_computer" onclick="choose_coin_side_a('rnd')" >(uporabi PRNG)</button>.<br>`;

            MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("ana_choosing_coin_side")]);
        }
    });
}

function playing_bojan() {
    document.getElementById("bojan_choosing_coin_side").innerHTML = `Izberi 
    <button id="choose_coin_side_b_head" onclick="choose_coin_side_b('head')" >moža \\((b_\\text{B} = 0)\\)</button> 
    ali <button id="choose_coin_side_b_tails" onclick="choose_coin_side_b('tails')" >cifro \\((b_\\text{B} = 1)\\)</button> 
    ali prepusti izbiro računalniku <br><button id="choose_coin_side_b_computer" onclick="choose_coin_side_b('rnd')" >(uporabi PRNG)</button>.<br>`;
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("bojan_choosing_coin_side")]);
}

function toHexString(byteArray) {
    return Array.from(byteArray, function (byte) {
        return ('0' + (byte & 0xFF).toString(16)).slice(-2);
    }).join('')
}
function toBinString(byteArray) {
    return Array.from(byteArray, function (byte) {
        return ('0000000' + (byte & 0xFF).toString(2)).slice(-8);
    }).join('')
}

function fromHexString(byteArray) {
    //todo
}


function random_512_bit_number() {
    randv = []
    for (var i = 0; i < 64; i++) {
        randv.push(Math.floor(Math.random() * 256));
    }
    return randv
}

function choose_coin_side_b(side) {
    if (side == 'head') {
        coin_side = 0;
    } else if (side == 'tails') {
        coin_side = 1;
    } else if (side == 'rnd') {
        coin_side = Math.floor(Math.random() * 2);
    }
    document.getElementById("choose_coin_side_b_head").disabled = true;
    document.getElementById("choose_coin_side_b_tails").disabled = true;
    document.getElementById("choose_coin_side_b_computer").disabled = true;
    document.getElementById("bojan_coin_side").innerHTML = 'Tvoja "izbira" je \\(b_\\text{B} = ' + coin_side + '\\).<br>';
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("bojan_coin_side")]);

    document.getElementById("bojan_calcualting_commitment_string").innerHTML =
        `<button id="choose_random_r" onclick="choose_random_r()">Izberi naključno</button>
     \\(511\\)-bitno stevilo \\(r\\) in 
     <button id="calculate_commitment_string" onclick="calculate_commitment_string()" disabled>izračunaj zaprisego</button> \\(c = \\text{SHA-256}(b_{\\text{B}}||r)\\).<br>`;
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("bojan_calcualting_commitment_string")]);

}


function choose_random_r() {
    randv = random_512_bit_number();
    randv[0] >>= 1; // removing first bit
    secret_r = randv;
    b_r = randv.slice(); // make a copy
    b_r[0] += coin_side << 7; // add coin_sice bit

    document.getElementById("bojan_commitment_string").innerHTML = `Prestavitev števila \\(b_{\\text{B}}||r\\) v šestnajstiški obiki je:<br>
    <textarea id="hex_box_br" rows="2" cols="64" oninput="convert_hex_br_to_bin()">`+ toHexString(b_r) + `</textarea><br>
    v binarnem zapisu pa: <br>
    <textarea id="bin_box_br" rows="8" cols="64" oninput="convert_bin_br_to_hex()">`+ toBinString(b_r) + `</textarea>.<br>`;
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("bojan_commitment_string")]);

    document.getElementById("calculate_commitment_string").disabled = false;
}

function calculate_commitment_string() {
    commitment_string = sha256(b_r);
    document.getElementById("bojan_sending_commitment_string").innerHTML = `Vrednost \\(c\\) zgoščevalne funkcije \\(\\text{SHA-256}\\) za \\(b_{\\text{B}}||r\\) v šestnajstišem zapisu je: <br>
    <textarea id="commitment_string_box" rows="1" cols="64" oninput="commitment_string_box()">`+ commitment_string + `</textarea><br>
    Izračunana vrendost \\(c\\) predstavlja zaprisego, ki jo lahko 
    <button id="send_commitment_strign_to_ana_button" onclick="send_commitment_strign_to_ana()">poslješ</button> <b>Ani</b>.`;
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("bojan_sending_commitment_string")]);

}


function convert_hex_br_to_bin() {

    str = document.getElementById("hex_box_br").value;
    str += '0'.repeat((2 - (str.length % 2)) % 2);

    var a = [];
    for (var i = 0, len = str.length; i < len; i += 2) {
        a.push(parseInt(str.substr(i, 2), 16));
    }
    b_r = a;
    console.log('hex', b_r)
    document.getElementById("bin_box_br").value = toBinString(b_r);
}
function convert_bin_br_to_hex() {
    str = document.getElementById("bin_box_br").value;
    str += '0'.repeat((8 - (str.length % 8)) % 8);
    var a = [];
    for (var i = 0, len = str.length; i < len; i += 8) {
        a.push(parseInt(str.substr(i, 8), 2));
    }
    b_r = a;
    console.log('bin', b_r)

    document.getElementById("hex_box_br").value = toHexString(b_r);
}

function commitment_string_box() {
    commitment_string = document.getElementById('commitment_string_box').value;
}

function send_commitment_strign_to_ana() {
    document.getElementById('send_commitment_strign_to_ana_button').disabled = true;
    document.getElementById('choose_random_r').disabled = true;
    document.getElementById('calculate_commitment_string').disabled = true;
    document.getElementById('hex_box_br').disabled = true;
    document.getElementById('bin_box_br').disabled = true;
    document.getElementById('commitment_string_box').disabled = true;
    paired_data_connection.on('data', function (data) {
        if (data['ana_coin_side'] != null) {
            opposite_coin_side = data['ana_coin_side'];
            document.getElementById('bojan_opeing_commitment').innerHTML = `<b>Ana</b> ti je poslala njeno "izbiro" \\(b_\\text{A} = ` + opposite_coin_side + `\\) za stran kovanca, zato ji lahko razkriješ<br> zaprisego oz. ji 
            <button id="send_decommintment_to_ana_button" onclick="send_decommintment_to_ana()">poslješ</button> \\(b_\\text{B}||r\\).`
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("bojan_opeing_commitment")]);

        }
    });
    paired_data_connection.send({ c: commitment_string });
    document.getElementById('bojan_waiting_coin_side_of_ana').innerHTML = `Počakaj na <b>Anino</b> "izbiro" \\(b_\\text{A}\\) za stran kovanca.<br>`;
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("bojan_waiting_coin_side_of_ana")]);

}

function choose_coin_side_a(side) {
    if (side == 'head') {
        coin_side = 0;
    } else if (side == 'tails') {
        coin_side = 1;
    } else if (side == 'rnd') {
        coin_side = Math.floor(Math.random() * 2);
    }
    document.getElementById("choose_coin_side_a_head").disabled = true;
    document.getElementById("choose_coin_side_a_tails").disabled = true;
    document.getElementById("choose_coin_side_a_computer").disabled = true;
    document.getElementById("ana_sending_coin_side").innerHTML = `Tvoja "izbira" je \\(b_\\text{A} = ` + coin_side + `\\), ki jo lahko 
    <button id="send_coin_side_to_bojan_button" onclick="send_coin_side_to_bojan()">poslješ</button>, kot čistopis <b>Bojanu</b>.<br>`;
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("ana_sending_coin_side")]);

}

function send_coin_side_to_bojan() {
    document.getElementById("send_coin_side_to_bojan_button").disabled = true;
    paired_data_connection.on('data', function (data) {
        if (data['bojan_b_r'] != null) {
            b_r = data['bojan_b_r'];

            document.getElementById("ana_reciving_decommitment").innerHTML = `<b>Bojan</b> ti je poslal razkritje zapirsege \\(b_{\\text{B}}||r\\), katere vrenost v šestnajstiškem zapisu <br> je sledeča:<br>
            <textarea id="hex_box_br" rows="2" cols="64" readonly>`+ toHexString(b_r) + `</textarea><br>
            v binarnem pa: <br>
            <textarea id="bin_box_br" rows="8" cols="64" readonly>`+ toBinString(b_r) + `</textarea>.<br>
            <br>Za veljavnost zaprisege moraš 
            <button id="check_decomitment_button" onclick="check_decomitment()">preveriti</button>,
             da število \\(b_{\\text{B}}||r\\) dolžine \\(512\\) bitov<br>in da velja \\(c \\equiv \\text{SHA-256}(b_{\\text{B}}||r)\\).<br>`;
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("ana_reciving_decommitment")]);

        }
    });
    paired_data_connection.send({ ana_coin_side: coin_side });
    document.getElementById("ana_waiting_for_decommintment").innerHTML = `Počakaj, da ti <b>Bojan</b> razkrije zaprisego oz.
    ti pošlje vrednost \\(b_{\\text{B}}||r\\) iz katere boš <br> lahko izvedela <b>Bojanovo</b> "izbiro" \\(b_\\text{B}\\) strani kovanca in preverila korektnost zaprisege.<br>`;
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("ana_waiting_for_decommintment")]);

}


function send_decommintment_to_ana() {
    document.getElementById("send_decommintment_to_ana_button").disabled = true;
    paired_data_connection.on('data', function (data) {
        if (data['end_of_protocol'] != null) {
            if (data['end_of_protocol'] == 'quit') {
                document.getElementById("final_verdict").innerHTML = '<b>Ana</b> se strinja s končnim rezultatom.';
            } else if (data['end_of_protocol'] == 'abort') {
                final_verdict = 'you_were_caught_cheating';
                document.getElementById("final_verdict").innerHTML = '<b>Ana</b> je odstopila od protokola, ker je zazala poizkus goljufije.<br>';
            }

            paired_data_connection.on('data', function (data) {
                if (data == 'repeat_the_protocol?')
                    document.getElementById("repeat_coin_flip").innerHTML = `Če želis lahko 
                        <button id="accept_repeat_protocol_button" onclick="accept_repeat_protocol(true)">sprejmeš</button> ali 
                        <button id="accept_repeat_protocol_button" onclick="accept_repeat_protocol(false)">zavrneš</button> povabilo od <b>Ane</b> na ponovno metanje <br> kovanca.`;
            });
            document.getElementById("repeat_coin_flip").innerHTML = 'Če želis lahko <button id="repeat_protocol_button" onclick="repeat_protocol()">povabiš</button> <b>Ano</b> na ponovno metanje kovanca.';

        }
    })
    paired_data_connection.send({ bojan_b_r: b_r });
    document.getElementById("bojan_calculating_result").innerHTML = `Skozi protokol si pridobil obe "izbiri" strani kovancev t.j. \\(b_\\text{A}\\) (<b>Anina</b> "izbira") in <br> \\(b_\\text{B}\\) (tvoja "izbira").<br>
    Zdaj lahko <button id="calculate_value_of_common_coin_side_button" onclick="calculate_value_of_common_coin_side_button()">izračunaš</button>
     stanje "skupnega" kovanca t.j. \\(b = b_\\text{A} \\oplus b_\\text{B}\\).`;
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("bojan_calculating_result")]);

}

function calculate_value_of_common_coin_side_button() {
    document.getElementById("calculate_value_of_common_coin_side_button").disabled = true;
    if (final_verdict == 'you_were_caught_cheating') {
        document.getElementById("final_result_of_coin_flip").innerHTML = 'Rezultat skupnega metanja kovanca je brezpredmeten (lažeš sam sebi), saj je <b>Ana</b> ugovila, da si skušal goljufati.<br>';
    }
    else {
        common_coin_side = coin_side ^ opposite_coin_side;
        document.getElementById("final_result_of_coin_flip").innerHTML = 'Rezultat skupnega metanja kovanca je \\(b = b_\\text{A} \\oplus b_\\text{B} = ' + opposite_coin_side + ' \\oplus ' + coin_side + ' = ' + common_coin_side + '\\).<br>';
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("final_result_of_coin_flip")]);

    }

}

function check_decomitment() {
    document.getElementById("check_decomitment_button").disabled = true;
    bit_length_of_b_r = 8 * b_r.length;
    hash_value_of_decommitment = sha256(b_r);

    document.getElementById("ana_testing_commitment").innerHTML = `Bitna dolžina \\(b_{\\text{B}}||r\\) je ` + bit_length_of_b_r + ` bitov.<br>
    Vrednost zgoščevalne funkcije \\(\\text{SHA-256}\\) za \\(b_{\\text{B}}||r\\) v šestnajstiškem zapisu je: <br>
    <textarea id="commitment_string_box" rows="1" cols="64" readonly>`+ hash_value_of_decommitment + `</textarea>.<br>
    <a href="https://blog.benjojo.co.uk/post/ssh-randomart-how-does-it-work-art">Randomart</a> slika vrednosti \\(\\text{SHA-256}(b_{\\text{B}}||r)\\) je: <br>`+
        randomart_of_hash_value(hash_value_of_decommitment) + `.<br>`;
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("ana_testing_commitment")]);


    if (hash_value_of_decommitment != recived_commitment_string || 8 * b_r.length != 512) {
        //cheating detected
        cheating_reason = ((8 * b_r.length != 512) ? `nepravilna dolžina razkritja zaprisege \\(b_{\\text{B}}||r\\) \\((` + 8 * b_r.length + `)\\)` : `nepravino razkritje zaprisege \\(\\text{SHA-256}(b_\\text{B}||r) \\neq c\\).`);
        document.getElementById("ana_calcuatling_result").innerHTML = `<br>Ker zaprisega ni veljavana, lahko <button id="cheating_detected_abort_button"
         onclick="cheating_detected_abort()">odstopiš</button> od protokola.`

    } else {
        opposite_coin_side = parseInt(document.getElementById("bin_box_br").value[0])
        document.getElementById("ana_calcuatling_result").innerHTML = `Prvi bit števila \\(b_{\\text{B}}||r\\) se skiriva <b>Bojanova</b> "izbira" \\(b_{\\text{B}} = ` + opposite_coin_side + `\\). 
        Tako si pridobila<br>obe "izbiri" strani kovancev t.j. \\(b_\\text{A}\\) (Tvoja "izbira") in \\(b_\\text{B}\\) (<b>Bojanova</b> "izbira").<br>
        Zdaj lahko <button id="calculate_value_of_common_coin_side_button" onclick="calculate_value_of_common_coin_side_button_ana()">izračunaš</button>
        stanje "skupnega" kovanca t.j. \\(b = b_\\text{A} \\oplus b_\\text{B}\\).`;
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("ana_calcuatling_result")]);
    }

}

function cheating_detected_abort() {
    document.getElementById("cheating_detected_abort_button").disabled = true;

    paired_data_connection.on('data', function (data) {
        if (data == 'repeat_the_protocol?')
            document.getElementById("repeat_coin_flip").innerHTML = `Če želis lahko 
                <button id="accept_repeat_protocol_button" onclick="accept_repeat_protocol(true)">sprejmeš</button> ali 
                <button id="accept_repeat_protocol_button" onclick="accept_repeat_protocol(false)">zavrneš</button> povabilo od <b>Bojana</b> na ponovno metanje <br> kovanca.`;
    });

    paired_data_connection.send({ end_of_protocol: 'abort' })
    document.getElementById("final_verdict").innerHTML = '<b>Bojan</b> je poskusl goljufati - ' + cheating_reason + '.';
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("final_verdict")]);

    document.getElementById("repeat_coin_flip").innerHTML = 'Če želis lahko <button id="repeat_protocol_button" onclick="repeat_protocol()">povabiš</button> <b>Bojana</b> na ponovno metanje kovanca.';
}

function calculate_value_of_common_coin_side_button_ana() {
    document.getElementById("calculate_value_of_common_coin_side_button").disabled = true;
    common_coin_side = coin_side ^ opposite_coin_side;
    document.getElementById("final_result_of_coin_flip").innerHTML = 'Rezultat skupnega metanja kovanca je \\(b = b_\\text{A} \\oplus b_\\text{B} = ' + coin_side + ' \\oplus ' + opposite_coin_side + ' = ' + common_coin_side + '\\).<br>';
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, document.getElementById("final_result_of_coin_flip")]);

    paired_data_connection.on('data', function (data) {
        if (data == 'repeat_the_protocol?')
            document.getElementById("repeat_coin_flip").innerHTML = `Če želis lahko 
                <button id="accept_repeat_protocol_button" onclick="accept_repeat_protocol(true)">sprejmeš</button> ali 
                <button id="accept_repeat_protocol_button" onclick="accept_repeat_protocol(false)">zavrneš</button> povabilo od <b>Bojana</b> na ponovno metanje <br> kovanca.`;
    });
    paired_data_connection.send({ end_of_protocol: 'quit' });
    document.getElementById("repeat_coin_flip").innerHTML = 'Če želis lahko <button id="repeat_protocol_button" onclick="repeat_protocol()">povabiš</button> <b>Bojana</b> na ponovno metanje kovanca.';
}

function gridToStringHTML(grid) {
    return '+' + '-'.repeat(grid[0].length) + '+<br>' +
        grid.map(function (line) { return '|' + line.join('') + '|' }).join('<br>') +
        '<br>+' + '-'.repeat(grid[0].length) + '+';
}


function randomart_of_hash_value(hash_string) {
    return '<tt class="monosp">' + gridToStringHTML(randomart.render(hash_string)).replace(/ /g, '\u00a0') + '</tt>';
}

function repeat_protocol() {
    repea_the_protocol = true;
    paired_data_connection.on('data', function (data) {
        if (data['repeat_protocol'] != null) {
            if (data['repeat_protocol'] == 'yes') {
                resset_all_and_start_all_over(true);
            } else {
                resset_all_and_start_all_over(false);
            }
        }
    });
    paired_data_connection.send('repeat_the_protocol?');
}
function accept_repeat_protocol(decision) {
    paired_data_connection.send({ repeat_protocol: (decision ? 'yes' : 'no') });
    resset_all_and_start_all_over(decision);
}

function resset_all_and_start_all_over(decision) {
    if (decision) {
        //start choosing Ana or Bojan
        role = null;
        coin_side = null;
        opposite_coin_side = null;
        recived_commitment_string = null;
        commitment_string = null;
        secret_r = null;
        b_r = null;
        final_verdict = null;
        cheating_reason = null;
        paired_data_connection.off('data');

        document.getElementById("izbira_vloge").innerHTML = '';

        document.getElementById("ana_waiting_for_commintment").innerHTML = '';
        document.getElementById("bojan_choosing_coin_side").innerHTML = '';
        document.getElementById("bojan_coin_side").innerHTML = '';
        document.getElementById("bojan_calcualting_commitment_string").innerHTML = '';
        document.getElementById("bojan_commitment_string").innerHTML = '';

        document.getElementById("bojan_sending_commitment_string").innerHTML = '';
        document.getElementById("ana_reciving_commitment_string").innerHTML = '';

        document.getElementById("bojan_waiting_coin_side_of_ana").innerHTML = '';
        document.getElementById("ana_choosing_coin_side").innerHTML = '';

        document.getElementById("ana_sending_coin_side").innerHTML = '';
        document.getElementById("bojan_opeing_commitment").innerHTML = '';

        document.getElementById("ana_waiting_for_decommintment").innerHTML = '';
        document.getElementById("ana_reciving_decommitment").innerHTML = '';
        document.getElementById("ana_testing_commitment").innerHTML = '';
        document.getElementById("ana_calcuatling_result").innerHTML = '';
        document.getElementById("bojan_calculating_result").innerHTML = '';

        document.getElementById("final_result_of_coin_flip").innerHTML = '';
        document.getElementById("final_verdict").innerHTML = '';
        document.getElementById("repeat_coin_flip").innerHTML = '';

        prepare_for_phase3();
    } else {
        location.reload(); //refresh the page
    }
}

function send_message() {
    var msg = document.getElementById("message_input").value
    if (msg.length > 0) {
        document.getElementById("chat_box").value += getTime() + ' ' + nickname + ' > ' + msg + '\n';
        document.getElementById("chat_box").scrollTop = document.getElementById("chat_box").scrollHeight;
        document.getElementById("message_input").value = '';
        paired_data_connection.send({ chat: msg });
    }
}

function getTime() {
    var d = new Date();
    return ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2);
}

function refresh_chat_box() {
    paired_data_connection.on('data', function (data) {
        if (data['chat'] != null && data['chat'].length != 0) {
            document.getElementById("chat_box").value += getTime() + ' ' + opposite_nickname + ' > ' + data['chat'] + '\n';
            document.getElementById("chat_box").scrollTop = document.getElementById("chat_box").scrollHeight;
        }
    })
    document.getElementById("chat_box").value = '';
    document.getElementById("chat_box").disabled = false;
    document.getElementById("message_input").disabled = false;
    document.getElementById("send_message_button").disabled = false;
}
