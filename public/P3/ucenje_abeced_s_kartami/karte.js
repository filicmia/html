//https://en.wikipedia.org/wiki/Glagolitic_script
//https://en.wikipedia.org/wiki/Slovene_alphabet
//https://en.wikipedia.org/wiki/Russian_alphabet
//https://en.wikipedia.org/wiki/Serbian_Cyrillic_alphabet
//https://en.wikipedia.org/wiki/Cyrillic_numerals <-- TODO
//srbska glagolica
var rusl = {
    'А': 'A',
    'Б': 'B',
    'В': 'V',
    'Г': 'G',
    'Д': 'D',
    'Е': 'JE',
    'Ё': 'JO',
    'Ж': 'Ž',
    'З': 'Z',
    'И': 'I',
    'Й': 'J',
    'К': 'K',
    'Л': 'L',
    'М': 'M',
    'Н': 'N',
    'О': 'O',
    'П': 'P',
    'Р': 'R',
    'С': 'S',
    'Т': 'T',
    'У': 'U',
    'Ф': 'F',
    'Х': 'H',
    'Ц': 'C',
    'Ч': 'Č',
    'Ш': 'Š',
    'Щ': 'Š',
    'Ъ': '',
  //  'Ъ': '\'',
    'Ы': 'I',
     'Ь': '',
   // 'Ь': '\"',
    'Э': 'E',
    'Ю': 'JU',
    'Я': 'JA',
};


var slru = {
   'A' : 'А' ,
   'B' : 'Б' ,
   'V' : 'В' ,
   'G' : 'Г' ,
   'D' : 'Д',
   'JE' : 'Е',
   'JO' : 'Ё',
   'Ž' : 'Ж' ,
   'Z' : 'З' ,
   'I' : 'И' ,
   'J' : 'Й' ,
   'K' : 'К' ,
   'L' : 'Л' ,
   'M' : 'М' ,
   'N' : 'Н' ,
   'O' : 'О' ,
   'P' : 'П' ,
   'R' : 'Р' ,
   'S' : 'С' ,
   'T' : 'Т' ,
   'U' : 'У' ,
   'F' : 'Ф' ,
   'H' : 'Х' ,
   'C' : 'Ц' ,
   'Č' : 'Ч' ,
   'Š' : 'Ш' ,
   'E' : 'Э' ,
   'JU' : 'Ю',
   'JA' : 'Я'
};


var srsl = {
    'А': 'A',
    'Б': 'B',
    'В': 'V',
    'Г': 'G',
    'Д': 'D',
    'Ђ': 'Đ',
    'Е': 'E',
    'Ж': 'Ž',
    'З': 'Z',
    'И': 'I',
    'Ј': 'J',
    'К': 'K',
    'Л': 'L',
    'Љ': 'LJ',
    'М': 'M',
    'Н': 'N',
    'Њ': 'NJ',
    'О': 'O',
    'П': 'P',
    'Р': 'R',
    'С': 'S',
    'Т': 'T',
    'Ћ': 'Ć',
    'У': 'U',
    'Ф': 'F',
    'Х': 'H',
    'Ц': 'C',
    'Ч': 'Č',
    'Џ': 'DŽ',
    'Ш': 'Š'
};

var slsr = {
    'A':'А',
    'B':'Б',
    'V':'В',
    'G':'Г',
    'D':'Д',
    'Đ':'Ђ',
    'E':'Е',
    'Ž':'Ж',
    'Z':'З',
    'I':'И',
    'J':'Ј',
    'K':'К',
    'L':'Л',
    'M':'М',
    'N':'Н',
    'NJ':'Њ',
    'O':'О',
    'P':'П',
    'R':'Р',
    'S':'С',
    'T':'Т',
    'Ć':'Ћ',
    'U':'У',
    'F':'Ф',
    'H':'Х',
    'C':'Ц',
    'Č':'Ч',
    'Š':'Ш',
    'DŽ': 'Џ',
    'LJ':'Љ'
};


var glsl ={
    'Ⰰ': 'A', 
    'Ⰱ': 'B', 
    'Ⰲ': 'V', 	
    'Ⰳ': 'G', 	
    'Ⰴ': 'D', 	
    'Ⰵ': 'E', 
    'Ⰶ': 'Ž', 
    'Ⰷ': 'DŽ', 
    'Ⰸ': 'Z', 
    'Ⰹ': 'I', 
    'Ⰺ': 'J',
    'Ⰻ': 'I', 
    'Ⰼ': 'Đ', 
    'Ⰽ': 'K', 
    'Ⰾ': 'L', 
    'Ⰿ': 'M', 
    'Ⱀ': 'N', 	
    'Ⱁ': 'O', 	
    'Ⱂ': 'P', 	
    'Ⱃ': 'R', 	
    'Ⱄ': 'S', 	
    'Ⱅ': 'T', 	
    'Ⱆ': 'U', 	
    'Ⱇ': 'F', 	
    'Ⱈ': 'H', 	
    'Ⱉ': 'O', 	
    'Ⱋ': 'Š', 	
    'Ⱌ': 'C', 	
    'Ⱍ': 'Č', 	
    'Ⱎ': 'Š', 	
    'Ⱏ': '', 
    'Ⱐ': '',
    'ⰟⰊ': 'I', 
    'Ⱐ': '',
    'Ⱑ': 'JA',
    'Ⱖ': 'JO',
    'Ⱓ':'JU',
    'Ⱔ':'E',
    'Ⱗ':'JE',
    'Ⱘ':'O',
    'Ⱙ':'JO',
    'Ⱚ':'TH',
    'Ⱛ':'I'
}

var slgl ={
    'A': 'Ⰰ', 
    'B': 'Ⰱ', 
    'V': 'Ⰲ', 	
    'G': 'Ⰳ', 	
    'D': 'Ⰴ', 	
    'E': 'Ⰵ', 
    'Ž': 'Ⰶ', 
    'Z': 'Ⰸ', 
    'I': 'Ⰹ',
    'J': 'Ⰺ',
    'K': 'Ⰽ', 
    'L': 'Ⰾ', 
    'M': 'Ⰿ', 
    'N': 'Ⱀ', 	
    'O': 'Ⱁ', 	
    'P': 'Ⱂ', 	
    'R': 'Ⱃ', 	
    'S': 'Ⱄ', 	
    'T': 'Ⱅ', 	
    'U': 'Ⱆ', 	
    'F': 'Ⱇ', 	
    'H': 'Ⱈ', 	
    'C': 'Ⱌ', 	
    'Č': 'Ⱍ', 	
    'Š': 'Ⱎ', 
    'DŽ': 'Ⰷ', 
    'JA': 'Ⱑ',
    'JO': 'Ⱖ',
    'JU': 'Ⱓ',
    'Đ': 'Ⰼ'
}




document.getElementById('card').style.display='none';
document.getElementById('c1').style.display='none';
document.getElementById('c2').style.display='none';
document.getElementById('c3').style.display='none';
document.getElementById('c4').style.display='none';
document.getElementById('c5').style.display='none';

function random_bin(){
    r = Math.random()
    if (r<.4)
        return 0;
    if (r<.7)
        return 1;
    if (r<.9)
        return 2;
    return 3;
}
var langdict = rusl;
var correct_card = 'c1';
var card_front_face = '';
var card_front_face_index = 0;
var bins = [];
var dictkeys = [];
var curr_bin =0;

function filter_invisible(langdict, ch){
    if (ch == 'Ⱐ' || ch == 'Ь'){
        return 'mehki znak'
    }
    if (ch == 'Ⱏ' || ch == 'Ъ'){
        return 'trdi znak'
    }
    return langdict[ch]
}
function cards(){
    var base = document.getElementsByName('seldict')[0].value;
    switch(base) {
        case 'slru':
            langdict=slru;
            break;
        case 'slsr':
            langdict=slsr;
            break;
        case 'slgl':
            langdict=slgl;
            break;
        case 'rusl':
            langdict=rusl;
            break;
        case 'srsl':
            langdict=srsl;
            break;
        case 'glsl':
            langdict=glsl;
            break;
        default:
            langdict = rusl;
    }

    //console.log(base);
    dictkeys=Object.keys(langdict);
    bins = [dictkeys.slice(), [], [], []];
    //console.log(bins);
    document.getElementById('card').style.display='block';
    document.getElementById('c1').style.display='inline';
    document.getElementById('c2').style.display='inline';
    document.getElementById('c3').style.display='inline';
    document.getElementById('c4').style.display='inline';
    document.getElementById('c5').style.display='inline';
    
    curr_bin =0;
    card_front_face_index=Math.floor(Math.random()*bins[curr_bin].length);
    card_front_face = bins[curr_bin][card_front_face_index];
    //console.log(card_front_face);
//    document.getElementById('card').innerHTML='<h1>'+ch+'</h1>';
    document.getElementById('card').innerHTML=card_front_face;
  
    document.getElementById('c1').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c2').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c3').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c4').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c5').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    correct_card = 'c'+Math.ceil(Math.random()*5);
    document.getElementById(correct_card).value = filter_invisible(langdict, card_front_face);

    document.getElementById('c1').innerHTML = '<h1>'+document.getElementById('c1').value+'</h1>';
    document.getElementById('c2').innerHTML = '<h1>'+document.getElementById('c2').value+'</h1>';
    document.getElementById('c3').innerHTML = '<h1>'+document.getElementById('c3').value+'</h1>';
    document.getElementById('c4').innerHTML = '<h1>'+document.getElementById('c4').value+'</h1>';
    document.getElementById('c5').innerHTML = '<h1>'+document.getElementById('c5').value+'</h1>';
    document.getElementById('info').innerHTML='';
}

function evalueate(id){
    var val = document.getElementById(id).value
    if (val == langdict[card_front_face]){
        bins[curr_bin].splice(card_front_face_index,1);
        bins[Math.min(3, curr_bin+1)].push(card_front_face);
        document.getElementById('info').innerHTML='<font color="MediumSeaGreen">Izbira JE bila pravilna.</font><br> Škatle: ' + [bins[0].length,bins[1].length,bins[2].length,bins[3].length].toString();

    }else{
        bins[curr_bin].splice(card_front_face_index,1);
        bins[Math.max(0, curr_bin-1)].push(card_front_face);
        document.getElementById('info').innerHTML='<font color="Tomato">Izbira NI bila pravilna</font>. Pravilna izbira za '+card_front_face+' je '+langdict[card_front_face]+'.<br> Škatle: ' + [bins[0].length,bins[1].length,bins[2].length,bins[3].length].toString();

    }

    curr_bin = random_bin();
    while(bins[curr_bin].length ==0){
        curr_bin = random_bin();
    }
    ////console.log('current bin sizes:', [bins[0].length,bins[1].length,bins[2].length,bins[3].length])

    card_front_face_index=Math.floor(Math.random()*bins[curr_bin].length);
    card_front_face = bins[curr_bin][card_front_face_index];
    ////console.log(card_front_face);

    document.getElementById('card').innerHTML=card_front_face;
  
    document.getElementById('c1').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c2').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c3').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c4').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    document.getElementById('c5').value = filter_invisible(langdict,dictkeys[Math.floor(Math.random()*dictkeys.length)]);
    correct_card = 'c'+Math.ceil(Math.random()*5);
    document.getElementById(correct_card).value = filter_invisible(langdict, card_front_face);

    document.getElementById('c1').innerHTML = '<h1>'+document.getElementById('c1').value+'</h1>';
    document.getElementById('c2').innerHTML = '<h1>'+document.getElementById('c2').value+'</h1>';
    document.getElementById('c3').innerHTML = '<h1>'+document.getElementById('c3').value+'</h1>';
    document.getElementById('c4').innerHTML = '<h1>'+document.getElementById('c4').value+'</h1>';
    document.getElementById('c5').innerHTML = '<h1>'+document.getElementById('c5').value+'</h1>';

}