window.addEventListener('load', function(){
   console.log('welcome to the website');
   console.log("EKOLO");
   function gcd(a, b){
      if(b==0)return a;
      return gcd(b, a%b);
   }
   
   var a, n, k, i;
   var reset=document.getElementById("reset");
   var ns=document.getElementById("ns");
   var table=document.getElementById('table'); 
   var mss=document.getElementById('message');
   var prvi;
   var list=document.getElementById('list');
   var stevec=-1;
   var stev1;
   var red=0;
   var redSt;
   
   reset.addEventListener('click', function(e){
      list.innerHTML="";
      for(var t=0; t<i; t++)table.deleteRow(0);
      mss.innerHTML="";
      a=parseInt(document.getElementById("a").value, 10);
      n=parseInt(document.getElementById("n").value, 10);
      var g=gcd(a, n);
      if(g!=1){
         mss.innerHTML="Najvecji skupni deljitelj stevil je "+ g+". Potreben pogoj, pa da obstaja multiplikativni red stevila je da sta si stevili tuji, to je da je nejcecji skupni deljitelj 1";
         i=-1;
         stev1=-1;
         return;
      }else{
         var row = table.insertRow(0);
         var c=row.insertCell(0);
         c.style.width='100px';
         c.innerHTML="Iteracija";
         c=row.insertCell(1);
         c.style.width='300px';
         c.innerHTML="Presnji_ostanek * a mod n";
         c=row.insertCell(2);
         c.style.width='300px';
         c.innerHTML='Produkt';
         i++;
         stev1++;
         k=1;
         i=1;
         stev1=1;
         prvi=true;
         
         //circle
         var k1=1;
         var st=0;
         red=0;
         do{
            red++;
            k1=(k1*a) % n;
            var newDiv = document.createElement("div"); 
            newDiv.setAttribute("class", "text");
            newDiv.innerHTML=k1.toString(10);
            var li = document.createElement("li");
            li.appendChild(newDiv);
            list.appendChild(li);
            st++;
         }while(k1!=1);
         redSt=st;
         console.log(st);
        if(st==1 || st==2 || st>30){
          for(var o=0; o<st; o++)list.removeChild(list.childNodes[0]);      
          document.getElementById("list").innerHTML="";
           return; 
         }
         var offset=360/st;
         var skw=-90+offset;
         var j=1;
         
         var stl=".circle {\nposition: relative;\nborder: 1px solid black;\npadding: 0;\nmargin: 1em auto;\nwidth: 20em;\nheight: 20em;\nborder-radius: 50%;\nlist-style: none;\noverflow: hidden;\n}\n";
         stl=stl+"li {\noverflow: hidden;\nposition: absolute;\ntop: -10%;\nright: -10%;\nwidth: 60%;\nheight: 60%;\ntransform-origin: 0% 100%;\n}\n";
         stl=stl+".text {\n position: absolute;\n left: -100%;\n width: 200%;\n height: 200%;\n text-align: center;\n transform: skewY("+(-skw)+"deg) rotate("+offset/2+"deg);\n padding-top: 30%;\n}\n";
         stl=stl+"li:first-child {\n transform: rotate(0deg) skewY("+skw+"deg);\n}\n";
         do{
            if(j!=1){
               stl=stl + 'li:nth-child('+ j  +') {\n transform: rotate('+ (j-1)*offset +'deg) skewY('+skw+'deg);\n}\n';
            }
            k=(k*a) % n;
            j++;
         }while(k!=1);
         stl=stl+"li .text {\nbackground: tomato;\n}\n";
         var styleSheet = document.createElement("style");
         styleSheet.type = "text/css";
         styleSheet.innerText = stl;
         document.head.appendChild(styleSheet);
         stevec=-1;
      }
   });
   
   //stackoverflow.com/questions/29006214/how-to-divide-a-circle-into-12-equal-parts-with-color-using-css3-javascript
   ns.addEventListener('click', function(e){
      if(i>20){
         if(table.rows[1].bgColor!='green'){
            i--;
            table.deleteRow(1);  
         }else{
            i--;
            table.deleteRow(2);
         }
      }
      if(stevec==-1 || stevec==red)stevec=0;
      var neki = document.querySelectorAll('.text');
      for(var q=0; q<neki.length && redSt!=1 && redSt!=2; q++){
         neki[q].style.background='tomato';
         if(q==stevec)neki[q].style.background='green';
      }
      stevec++;
      if(i>0){
         var row=table.insertRow(i);
         var c=row.insertCell(0);
         c.innerHTML=stev1;
         c=row.insertCell(1);
         c.innerHTML=k+" * "+a+" % "+n;
         c=row.insertCell(2);
         c.innerHTML=k* a+" % "+n+" = " + (k*a)%n;
         k=(k*a)%n;
         if(k!=1)row.bgColor='red';
         else{
            if(prvi==true){
               prvi=false;
               c=row.insertCell(3);
               c.innerHTML="Dobili smo multiplikativni red stevila!";
               c.bgColor="white";
               row.bgColor='green';
            }else{
               row.bgColor='silver';  
            }
         } 
         i++;
         stev1++;
      }
   });
})