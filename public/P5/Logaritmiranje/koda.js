window.addEventListener('load', function(){
    var a, n, st, cikel, indPrviCikel; //cikel:bool nam pove, ce se je ze pojavil cikel, indPrviCikel indeks, kjer se pojavi prvi cikel
    var vrednostCikla; // vrednost, pri kateri se pojavi cikel
    var color; //barva s katero barvamo
    var reset=document.getElementById("reset");
    var ns=document.getElementById("ns");
    var tab=document.getElementById("table");
    var urejena=document.getElementById("tableUrejena");
    var mess=document.getElementById("mess");
    var opacity=1;
    
    reset.addEventListener('click', function(e){
        color="A6E1A1";
        vrednostCikla=-1;
        table.style.marginRight="30px"
        cikel=false;
        mess.innerHTML="";
        for(var i=0; i<st; i++){
          if(urejena.rows[0])urejena.deleteRow(0);
          tab.deleteRow(0);
        }
        a=document.getElementById("a").value;
        n=document.getElementById("n").value;
        st=1;
        var row=tab.insertRow(0);
        var c=row.insertCell(0);
        var str="k";
        str=str.bold();
        c.innerHTML=str;
        c.style.width='100px';
        c=row.insertCell(1);
        str="c";
        str=str.bold();
        c.innerHTML=str;
        c.style.width='100px';
        
        row=urejena.insertRow(0);
        c=row.insertCell(0);
        c.innerHTML='k';
        c.style.width='100px';
        c=row.insertCell(1);
        c.innerHTML='c';
        c.style.width='100px';
    });
    
    ns.addEventListener('click', function(e){
        if(st==1){
            var row=tab.insertRow(1);
            var c=row.insertCell(0);
            c.style.width='100px';
            c.innerHTML='0';
            c=row.insertCell(1);
            c.innerHTML='1';
            c.style.width='100px';
            row=urejena.insertRow(1);
            c=row.insertCell(0);
            c.style.width='100px';
            c.innerHTML='0';
            c=row.insertCell(1);
            c.innerHTML='1';
            c.style.width='100px';
            st++;
        }else{  
            mess.innerHTML="";
            var row=tab.insertRow(st);
            
            //nastavimo prvo celico... presnja +1
            var presnji=tab.rows[st-1].cells[0].innerHTML;
            presnji=parseInt(presnji, 10);
            presnji++;
            var c=row.insertCell(0);
            c.style.width='100px';
            c.innerHTML=presnji;
            
            //nastavimo drugo celico
            presnji=tab.rows[st-1].cells[1].innerHTML;
            presnji=parseInt(presnji, 10);
            presnji=(presnji*a) % n;
            c=row.insertCell(1);
            c.style.width='100px';
            c.innerHTML=presnji;

            //po potrebi pobarvamo za oznaciti nov cikel
            if(cikel==true){
                if(presnji==vrednostCikla)toggleColor();
                row.style.backgroundColor=color;
            }

            //housekeeping
            if(cikel==true && st>=indPrviCikel+10){
                console.log("ekolo " + indPrviCikel);
                tab.deleteRow(indPrviCikel-1);
            }else
                st++;


            //popravimo se urejeno tabelo in preverimo, ce je prislo do cikla
            if(cikel==false){    

                //preverimo, ce je pri tem koraku prislo do cikla
                var trenutna=tab.rows[st-1].cells[1].innerHTML;
                trenutna=parseInt(trenutna, 10);
                for(var i=1; i<st-1; i++){
                    var vrednost=parseInt(tab.rows[i].cells[1].innerHTML, 10);
                    if(vrednost==trenutna){
                        vrednostCikla=vrednost;
                        indPrviCikel=st;
                        cikel=true;

                        tab.rows[i].style.backgroundColor="green";
                        tab.rows[st-1].style.backgroundColor="green";
                        for(var j=i+1; j<st-1; j++){
                            tab.rows[j].style.backgroundColor="lime";
                        }
                    }
                }

                if(cikel==false){
                    var data=parseInt(tab.rows[st-1].cells[1].innerHTML, 10);
                    for(var o=1; o<st-1; o++){
                      console.log(data+"  "+parseInt(urejena.rows[o].cells[1].innerHTML));
                      if(parseInt(urejena.rows[o].cells[1].innerHTML)>data){
                        console.log("vnesemo"+o);
                        row=urejena.insertRow(o);
                        c=row.insertCell(0);
                        c.innerHTML=st-2;
                        c=row.insertCell(1);
                        c.innerHTML=data;
                        return;
                      }
                    }
                    row=urejena.insertRow(st-1);
                    c=row.insertCell(0);
                    c.innerHTML=st-2;
                    c=row.insertCell(1);
                    c.innerHTML=data;
                }
            }
            return;
        }
    });

    function toggleColor(){
        if(color=='A9F7A3'){
            color='A6E1A1'
        }else color='A9F7A3';
    }
})