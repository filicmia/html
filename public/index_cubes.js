// Global variable definitionvar canvas;
var canvas;
var gl;
var shaderProgram;

//Links
var linkP1 = "../gesla-in-pini.html";
var linkP2 = "../kode.html";
var linkP3 = "../zgoscevalne-funkciije.html";
var linkP4 = "../sifriranje.html";
var linkP5 = "../racunala.html";
var linkP6 = "../racunala.html";
var linkP7 = "../racunala.html";
var linkP8 = "../protokoli.html";
var linkP9 = "../napadi.html";

// Buffers for all 9 cubes:

//Helper buffer
var cubeVertexPositions;
var cubeNormals;
var cubeTextureCoords;
var cubeIndices;

//P1: Gesla & Virusi / Racunalniska varnost
var p1VertexPositionBuffer;
var p1VertexNormalBuffer;
var p1VertexTextureCoordBuffer;
var p1VertexIndexBuffer;

//P2: Okrajsave / Zgoscevalne funkcije
var p2VertexPositionBuffer;
var p2VertexNormalBuffer;
var p2VertexTextureCoordBuffer;
var p2VertexIndexBuffer;

//P3: Kode / Teorija kodiranja
var p3VertexPositionBuffer;
var p3VertexNormalBuffer;
var p3VertexTextureCoordBuffer;
var p3VertexIndexBuffer;

//P4: Sifre / Simetricni kriptosistemi
var p4VertexPositionBuffer;
var p4VertexNormalBuffer;
var p4VertexTextureCoordBuffer;
var p4VertexIndexBuffer;

//P5: Racunala / 
var p5VertexPositionBuffer;
var p5VertexNormalBuffer;
var p5VertexTextureCoordBuffer;
var p5VertexIndexBuffer;

//P6: Podpisi / Asimetricni kriptosistemi
var p6VertexPositionBuffer;
var p6VertexNormalBuffer;
var p6VertexTextureCoordBuffer;
var p6VertexIndexBuffer;

//P7: Protokoli /
var p7VertexPositionBuffer;
var p7VertexNormalBuffer;
var p7VertexTextureCoordBuffer;
var p7VertexIndexBuffer;

//P8: Loto / Generator nakljucnih stevil
var p8VertexPositionBuffer;
var p8VertexNormalBuffer;
var p8VertexTextureCoordBuffer;
var p8VertexIndexBuffer;

//P9: Verige / Blocne verige
var p9VertexPositionBuffer;
var p9VertexNormalBuffer;
var p9VertexTextureCoordBuffer;
var p9VertexIndexBuffer;

// Model-view and projection matrix and model-view matrix stack
var mvMatrixStack = [];
var mvMatrix = mat4.create();
var pMatrix = mat4.create();

// Variable for storing textures
var p1Texture;
var p2Texture;
var p3Texture;
var p4Texture;
var p5Texture;
var p6Texture;
var p7Texture;
var p8Texture;
var p9Texture;

// Variable that stores the loading state of textures.
var numberOfTextures = 9;
var texturesLoaded = 0;

// Variables for storing curent rotation of cubes
var rotationP1 = 0;
var rotationP2 = 0;
var rotationP3 = 0;
var rotationP4 = 0;
var rotationP5 = 0;
var rotationP6 = 0;
var rotationP7 = 0;
var rotationP8 = 0;
var rotationP9 = 0;

// Variables to see if cubes should rotate
var rotateP1 = 1;
var rotateP2 = 1;
var rotateP3 = 1;
var rotateP4 = 1;
var rotateP5 = 1;
var rotateP6 = 1;
var rotateP7 = 1;
var rotateP8 = 1;
var rotateP9 = 1;

//states of cubes - 0 for simple face, 1 for complex face
var stanjeP1 = 0;
var stanjeP2 = 0;
var stanjeP3 = 0;
var stanjeP4 = 0;
var stanjeP5 = 0;
var stanjeP6 = 0;
var stanjeP7 = 0;
var stanjeP8 = 0;
var stanjeP9 = 0;

var rotateXZ = 0.0;

// Mouse helper variables
var ndcX = null;
var ndcY = null;

// Helper variable for animation
var lastTime = 0;

// Timeout helper
var myTimeout;

// Cube position X constants
const colOneX_left = -0.4464285714285714;
const colOneX_right = -0.1785714285714286;

const colTwoX_left = -0.11785714285714288;
const colTwoX_right = 0.1166666666666667;

const colThreeX_left = 0.17738095238095242;
const colThreeX_right = 0.4452380952380952;

// Cube position Y constants
const lineOneY_up = 0.8963616317530321;
const lineOneY_down = 0.35611907386990094;

const lineTwoY_up = 0.24145534729878726;
const lineTwoY_down = -0.2370452039691291;

const lineThreeY_up = -0.3472987872105844;
const lineThreeY_down = -0.8941565600882029;



//
// Matrix utility functions
//
// mvPush   ... push current matrix on matrix stack
// mvPop    ... pop top matrix from stack
// degToRad ... convert degrees to radians
//
function mvPushMatrix() {
  var copy = mat4.create();
  mat4.set(mvMatrix, copy);
  mvMatrixStack.push(copy);
}

function mvPopMatrix() {
  if (mvMatrixStack.length == 0) {
    throw "Invalid popMatrix!";
  }
  mvMatrix = mvMatrixStack.pop();
}

function degToRad(degrees) {
  return degrees * Math.PI / 180;
}

//
// initGL
//
// Initialize WebGL, returning the GL context or null if
// WebGL isn't available or could not be initialized.
//
function initGL(canvas) {
  var gl = null;
  try {
    // Try to grab the standard context. If it fails, fallback to experimental.
    gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
  } catch(e) {}

  // If we don't have a GL context, give up now
  if (!gl) {
    alert("Unable to initialize WebGL. Your browser may not support it.");
  }
  return gl;
}

//
// getShader
//
// Loads a shader program by scouring the current document,
// looking for a script with the specified ID.
//
function getShader(gl, id) {
  var shaderScript = document.getElementById(id);

  // Didn't find an element with the specified ID; abort.
  if (!shaderScript) {
    return null;
  }
  
  // Walk through the source element's children, building the
  // shader source string.
  var shaderSource = "";
  var currentChild = shaderScript.firstChild;
  while (currentChild) {
    if (currentChild.nodeType == 3) {
        shaderSource += currentChild.textContent;
    }
    currentChild = currentChild.nextSibling;
  }
  
  // Now figure out what type of shader script we have,
  // based on its MIME type.
  var shader;
  if (shaderScript.type == "x-shader/x-fragment") {
    shader = gl.createShader(gl.FRAGMENT_SHADER);
  } else if (shaderScript.type == "x-shader/x-vertex") {
    shader = gl.createShader(gl.VERTEX_SHADER);
  } else {
    return null;  // Unknown shader type
  }

  // Send the source to the shader object
  gl.shaderSource(shader, shaderSource);

  // Compile the shader program
  gl.compileShader(shader);

  // See if it compiled successfully
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    alert(gl.getShaderInfoLog(shader));
    return null;
  }

  return shader;
}

//
// initShaders
//
// Initialize the shaders, so WebGL knows how to light our scene.
//
function initShaders() {
    var fragmentShader = getShader(gl, "per-fragment-lighting-fs");
    var vertexShader = getShader(gl, "per-fragment-lighting-vs");
  
  // Create the shader program
  shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader);
  gl.attachShader(shaderProgram, fragmentShader);
  gl.linkProgram(shaderProgram);
  
  // If creating the shader program failed, alert
  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
    alert("Unable to initialize the shader program.");
  }

  // start using shading program for rendering
  gl.useProgram(shaderProgram);

  // store location of aVertexPosition variable defined in shader
  shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");

  // turn on vertex position attribute at specified position
  gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

  // store location of vertex normals variable defined in shader
  shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");

  // turn on vertex normals attribute at specified position
  gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);

  // store location of texture coordinate variable defined in shader
  shaderProgram.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");

  // turn on texture coordinate attribute at specified position
  gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);

  // store location of uPMatrix variable defined in shader - projection matrix 
  shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
  // store location of uMVMatrix variable defined in shader - model-view matrix 
  shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
  // store location of uNMatrix variable defined in shader - normal matrix 
  shaderProgram.nMatrixUniform = gl.getUniformLocation(shaderProgram, "uNMatrix");
  // store location of uSampler variable defined in shader
  shaderProgram.samplerUniform = gl.getUniformLocation(shaderProgram, "uSampler");
  // store location of uMaterialShininess variable defined in shader
  shaderProgram.materialShininessUniform = gl.getUniformLocation(shaderProgram, "uMaterialShininess");
  // store location of uAmbientColor variable defined in shader
  shaderProgram.ambientColorUniform = gl.getUniformLocation(shaderProgram, "uAmbientColor");
  // store location of uPointLightingLocation variable defined in shader
  shaderProgram.pointLightingLocationUniform = gl.getUniformLocation(shaderProgram, "uPointLightingLocation");
  // store location of uPointLightingSpecularColor variable defined in shader
  shaderProgram.pointLightingSpecularColorUniform = gl.getUniformLocation(shaderProgram, "uPointLightingSpecularColor");
  // store location of uPointLightingDiffuseColor variable defined in shader
  shaderProgram.pointLightingDiffuseColorUniform = gl.getUniformLocation(shaderProgram, "uPointLightingDiffuseColor");
}

//
// setMatrixUniforms
//
// Set the uniform values in shaders for model-view and projection matrix.
//
function setMatrixUniforms() {
  gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
  gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);

  var normalMatrix = mat3.create();
  mat4.toInverseMat3(mvMatrix, normalMatrix);
  mat3.transpose(normalMatrix);
  gl.uniformMatrix3fv(shaderProgram.nMatrixUniform, false, normalMatrix);
}

//
// initTextures
//
// Initialize the textures we'll be using, then initiate a load of
// the texture images. The handleTextureLoaded() callback will finish
// the job; it gets called each time a texture finishes loading.
//
function initTextures() {
  //P1: Gesla & Virusi / Racunalniska varnost
  p1Texture = gl.createTexture();
  p1Texture.image = new Image();
  p1Texture.image.onload = function () {
    handleTextureLoaded(p1Texture)
  }
  p1Texture.image.src = "./assets/gesla&virusi_tekstura.jpg";

  p2Texture = gl.createTexture();
  p2Texture.image = new Image();
  p2Texture.image.onload = function () {
    handleTextureLoaded(p2Texture)
  }
  p2Texture.image.src = "./assets/okrajsave_tekstura.jpg";

  p3Texture = gl.createTexture();
  p3Texture.image = new Image();
  p3Texture.image.onload = function () {
    handleTextureLoaded(p3Texture)
  }
  p3Texture.image.src = "./assets/kode_tekstura.jpg";

  p4Texture = gl.createTexture();
  p4Texture.image = new Image();
  p4Texture.image.onload = function () {
    handleTextureLoaded(p4Texture)
  }
  p4Texture.image.src = "./assets/sifre_tekstura.jpg";

  p5Texture = gl.createTexture();
  p5Texture.image = new Image();
  p5Texture.image.onload = function () {
    handleTextureLoaded(p5Texture)
  }
  p5Texture.image.src = "./assets/racunala_tekstura.jpg";

  p6Texture = gl.createTexture();
  p6Texture.image = new Image();
  p6Texture.image.onload = function () {
    handleTextureLoaded(p6Texture)
  }
  p6Texture.image.src = "./assets/podpisi_tekstura.jpg";

  p7Texture = gl.createTexture();
  p7Texture.image = new Image();
  p7Texture.image.onload = function () {
    handleTextureLoaded(p7Texture)
  }
  p7Texture.image.src = "./assets/protokoli_tekstura.jpg";

  p8Texture = gl.createTexture();
  p8Texture.image = new Image();
  p8Texture.image.onload = function () {
    handleTextureLoaded(p8Texture)
  }
  p8Texture.image.src = "./assets/loto_tekstura.jpg";

  p9Texture = gl.createTexture();
  p9Texture.image = new Image();
  p9Texture.image.onload = function () {
    handleTextureLoaded(p9Texture)
  }
  p9Texture.image.src = "./assets/verige_tekstura.jpg";
}

function handleTextureLoaded(texture) {
  gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

  // Third texture usus Linear interpolation approximation with nearest Mipmap selection
  gl.bindTexture(gl.TEXTURE_2D, texture);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  gl.generateMipmap(gl.TEXTURE_2D);

  gl.bindTexture(gl.TEXTURE_2D, null);

  // when texture loading is finished we can draw scene.
  texturesLoaded += 1;
}

//
// handleLoadedTeapot
//
// Handle loaded teapot
//
function handleLoadedCube(cubeData) {


  window.localStorage.setItem('cubeV', JSON.stringify(cubeData.vertexPositions));
  window.localStorage.setItem('cubeN', JSON.stringify(cubeData.vertexNormals));
  window.localStorage.setItem('cubeT', JSON.stringify(cubeData.vertexTextureCoords));
  window.localStorage.setItem('cubeI', JSON.stringify(cubeData.indices));

  document.getElementById("loadingtext").textContent = "";
}

//
// loadTeapot
//
// Load teapot
//
function loadCube() {
  var request = new XMLHttpRequest();
  request.open("GET", "./assets/kocka3.json");
  request.onreadystatechange = function () {
    if (request.readyState == 4) {
      handleLoadedCube(JSON.parse(request.responseText));
    }
  }
  request.send();
}

function initBuffers() {

cubeVertexPositions = JSON.parse(window.localStorage.getItem('cubeV'));
cubeNormals = JSON.parse(window.localStorage.getItem('cubeN'));
cubeTextureCoords = JSON.parse(window.localStorage.getItem('cubeT'));
cubeIndices = JSON.parse(window.localStorage.getItem('cubeI'));

// P1:
  //Pass the vertex positions into WebGL
  p1VertexPositionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p1VertexPositionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVertexPositions), gl.STATIC_DRAW);
  p1VertexPositionBuffer.itemSize = 3;
  p1VertexPositionBuffer.numItems = cubeVertexPositions.length / 3;

  //Pass the normals into WebGL
  p1VertexNormalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p1VertexNormalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeNormals), gl.STATIC_DRAW);
  p1VertexNormalBuffer.itemSize = 3;
  p1VertexNormalBuffer.numItems = cubeNormals.length / 3;
  
  //Pass the texture coords into WebGL
  p1VertexTextureCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p1VertexTextureCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeTextureCoords), gl.STATIC_DRAW);
  p1VertexTextureCoordBuffer.itemSize = 2;
  p1VertexTextureCoordBuffer.numItems = cubeTextureCoords.length / 2;

  //Pass the indices into WebGL
  p1VertexIndexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p1VertexIndexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeIndices), gl.STATIC_DRAW);
  p1VertexIndexBuffer.itemSize = 1;
  p1VertexIndexBuffer.numItems = cubeIndices.length;
//-----------------------------------------------------------------------------------------

//P2:
  //Pass the vertex positions into WebGL
  p2VertexPositionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p2VertexPositionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVertexPositions), gl.STATIC_DRAW);
  p2VertexPositionBuffer.itemSize = 3;
  p2VertexPositionBuffer.numItems = cubeVertexPositions.length / 3;

  //Pass the normals into WebGL
  p2VertexNormalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p2VertexNormalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeNormals), gl.STATIC_DRAW);
  p2VertexNormalBuffer.itemSize = 3;
  p2VertexNormalBuffer.numItems = cubeNormals.length / 3;

  //Pass the texture coords into WebGL
  p2VertexTextureCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p2VertexTextureCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeTextureCoords), gl.STATIC_DRAW);
  p2VertexTextureCoordBuffer.itemSize = 2;
  p2VertexTextureCoordBuffer.numItems = cubeTextureCoords.length / 2;

  //Pass the indices into WebGL
  p2VertexIndexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p2VertexIndexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeIndices), gl.STATIC_DRAW);
  p2VertexIndexBuffer.itemSize = 1;
  p2VertexIndexBuffer.numItems = cubeIndices.length;
//-----------------------------------------------------------------------------------------

//P3:
  //Pass the vertex positions into WebGL
  p3VertexPositionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p3VertexPositionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVertexPositions), gl.STATIC_DRAW);
  p3VertexPositionBuffer.itemSize = 3;
  p3VertexPositionBuffer.numItems = cubeVertexPositions.length / 3;

  //Pass the normals into WebGL
  p3VertexNormalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p3VertexNormalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeNormals), gl.STATIC_DRAW);
  p3VertexNormalBuffer.itemSize = 3;
  p3VertexNormalBuffer.numItems = cubeNormals.length / 3;

  //Pass the texture coords into WebGL
  p3VertexTextureCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p3VertexTextureCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeTextureCoords), gl.STATIC_DRAW);
  p3VertexTextureCoordBuffer.itemSize = 2;
  p3VertexTextureCoordBuffer.numItems = cubeTextureCoords.length / 2;

  //Pass the indices into WebGL
  p3VertexIndexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p3VertexIndexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeIndices), gl.STATIC_DRAW);
  p3VertexIndexBuffer.itemSize = 1;
  p3VertexIndexBuffer.numItems = cubeIndices.length;
//-----------------------------------------------------------------------------------------

//P4:
  //Pass the vertex positions into WebGL
  p4VertexPositionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p4VertexPositionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVertexPositions), gl.STATIC_DRAW);
  p4VertexPositionBuffer.itemSize = 3;
  p4VertexPositionBuffer.numItems = cubeVertexPositions.length / 3;

  //Pass the normals into WebGL
  p4VertexNormalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p4VertexNormalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeNormals), gl.STATIC_DRAW);
  p4VertexNormalBuffer.itemSize = 3;
  p4VertexNormalBuffer.numItems = cubeNormals.length / 3;

  //Pass the texture coords into WebGL
  p4VertexTextureCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p4VertexTextureCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeTextureCoords), gl.STATIC_DRAW);
  p4VertexTextureCoordBuffer.itemSize = 2;
  p4VertexTextureCoordBuffer.numItems = cubeTextureCoords.length / 2;

  //Pass the indices into WebGL
  p4VertexIndexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p4VertexIndexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeIndices), gl.STATIC_DRAW);
  p4VertexIndexBuffer.itemSize = 1;
  p4VertexIndexBuffer.numItems = cubeIndices.length;
//-----------------------------------------------------------------------------------------

//P5:
  //Pass the vertex positions into WebGL
  p5VertexPositionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p5VertexPositionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVertexPositions), gl.STATIC_DRAW);
  p5VertexPositionBuffer.itemSize = 3;
  p5VertexPositionBuffer.numItems = cubeVertexPositions.length / 3;

  //Pass the normals into WebGL
  p5VertexNormalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p5VertexNormalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeNormals), gl.STATIC_DRAW);
  p5VertexNormalBuffer.itemSize = 3;
  p5VertexNormalBuffer.numItems = cubeNormals.length / 3;

  //Pass the texture coords into WebGL
  p5VertexTextureCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p5VertexTextureCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeTextureCoords), gl.STATIC_DRAW);
  p5VertexTextureCoordBuffer.itemSize = 2;
  p5VertexTextureCoordBuffer.numItems = cubeTextureCoords.length / 2;

  //Pass the indices into WebGL
  p5VertexIndexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p5VertexIndexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeIndices), gl.STATIC_DRAW);
  p5VertexIndexBuffer.itemSize = 1;
  p5VertexIndexBuffer.numItems = cubeIndices.length;
//-----------------------------------------------------------------------------------------

//P6:
  //Pass the vertex positions into WebGL
  p6VertexPositionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p6VertexPositionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVertexPositions), gl.STATIC_DRAW);
  p6VertexPositionBuffer.itemSize = 3;
  p6VertexPositionBuffer.numItems = cubeVertexPositions.length / 3;

  //Pass the normals into WebGL
  p6VertexNormalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p6VertexNormalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeNormals), gl.STATIC_DRAW);
  p6VertexNormalBuffer.itemSize = 3;
  p6VertexNormalBuffer.numItems = cubeNormals.length / 3;

  //Pass the texture coords into WebGL
  p6VertexTextureCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p6VertexTextureCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeTextureCoords), gl.STATIC_DRAW);
  p6VertexTextureCoordBuffer.itemSize = 2;
  p6VertexTextureCoordBuffer.numItems = cubeTextureCoords.length / 2;

  //Pass the indices into WebGL
  p6VertexIndexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p6VertexIndexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeIndices), gl.STATIC_DRAW);
  p6VertexIndexBuffer.itemSize = 1;
  p6VertexIndexBuffer.numItems = cubeIndices.length;
//-----------------------------------------------------------------------------------------

//P7:
  //Pass the vertex positions into WebGL
  p7VertexPositionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p7VertexPositionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVertexPositions), gl.STATIC_DRAW);
  p7VertexPositionBuffer.itemSize = 3;
  p7VertexPositionBuffer.numItems = cubeVertexPositions.length / 3;

  //Pass the normals into WebGL
  p7VertexNormalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p7VertexNormalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeNormals), gl.STATIC_DRAW);
  p7VertexNormalBuffer.itemSize = 3;
  p7VertexNormalBuffer.numItems = cubeNormals.length / 3;

  //Pass the texture coords into WebGL
  p7VertexTextureCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p7VertexTextureCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeTextureCoords), gl.STATIC_DRAW);
  p7VertexTextureCoordBuffer.itemSize = 2;
  p7VertexTextureCoordBuffer.numItems = cubeTextureCoords.length / 2;

  //Pass the indices into WebGL
  p7VertexIndexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p7VertexIndexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeIndices), gl.STATIC_DRAW);
  p7VertexIndexBuffer.itemSize = 1;
  p7VertexIndexBuffer.numItems = cubeIndices.length;
//-----------------------------------------------------------------------------------------

//P8:
  //Pass the vertex positions into WebGL
  p8VertexPositionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p8VertexPositionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVertexPositions), gl.STATIC_DRAW);
  p8VertexPositionBuffer.itemSize = 3;
  p8VertexPositionBuffer.numItems = cubeVertexPositions.length / 3;

  //Pass the normals into WebGL
  p8VertexNormalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p8VertexNormalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeNormals), gl.STATIC_DRAW);
  p8VertexNormalBuffer.itemSize = 3;
  p8VertexNormalBuffer.numItems = cubeNormals.length / 3;

  //Pass the texture coords into WebGL
  p8VertexTextureCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p8VertexTextureCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeTextureCoords), gl.STATIC_DRAW);
  p8VertexTextureCoordBuffer.itemSize = 2;
  p8VertexTextureCoordBuffer.numItems = cubeTextureCoords.length / 2;

  //Pass the indices into WebGL
  p8VertexIndexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p8VertexIndexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeIndices), gl.STATIC_DRAW);
  p8VertexIndexBuffer.itemSize = 1;
  p8VertexIndexBuffer.numItems = cubeIndices.length;
//-----------------------------------------------------------------------------------------

//P9:
  //Pass the vertex positions into WebGL
  p9VertexPositionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p9VertexPositionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVertexPositions), gl.STATIC_DRAW);
  p9VertexPositionBuffer.itemSize = 3;
  p9VertexPositionBuffer.numItems = cubeVertexPositions.length / 3;

  //Pass the normals into WebGL
  p9VertexNormalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p9VertexNormalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeNormals), gl.STATIC_DRAW);
  p9VertexNormalBuffer.itemSize = 3;
  p9VertexNormalBuffer.numItems = cubeNormals.length / 3;

  //Pass the texture coords into WebGL
  p9VertexTextureCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, p9VertexTextureCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeTextureCoords), gl.STATIC_DRAW);
  p9VertexTextureCoordBuffer.itemSize = 2;
  p9VertexTextureCoordBuffer.numItems = cubeTextureCoords.length / 2;

  //Pass the indices into WebGL
  p9VertexIndexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p9VertexIndexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeIndices), gl.STATIC_DRAW);
  p9VertexIndexBuffer.itemSize = 1;
  p9VertexIndexBuffer.numItems = cubeIndices.length;
//-----------------------------------------------------------------------------------------
}


//
// drawScene
//
// Draw the scene.
//
function drawScene() {
  resize(gl.canvas);

  // set the rendering environment to full canvas size
  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
  // Clear the canvas before we start drawing on it.
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  //if (p1VertexPositionBuffer == null || p1VertexNormalBuffer == null || p1VertexTextureCoordBuffer == null || p1VertexIndexBuffer == null) {
  //  return;
  //}
  
  // Establish the perspective with which we want to view the
  // scene. Our field of view is 45 degrees, with a width/height
  // ratio of 640:480, and we only want to see objects between 0.1 units
  // and 100 units away from the camera.
  mat4.perspective(50, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);

  // set uniforms for lights as defined in the document
    gl.uniform3f(
      shaderProgram.ambientColorUniform,
      0.2,
      0.2,
      0.2
    );

    gl.uniform3f(
      shaderProgram.pointLightingLocationUniform,
      1.0,
      1.0,
      5.0
    );

    gl.uniform3f(
      shaderProgram.pointLightingSpecularColorUniform,
      0.8,
      0.8,
      0.8
    );

    gl.uniform3f(
      shaderProgram.pointLightingDiffuseColorUniform,
      0.8,
      0.8,
      0.8
    );

  // Activate shininess
  gl.uniform1f(shaderProgram.materialShininessUniform, 50.0);

  // Set the drawing position to the "identity" point, which is
  // the center of the scene.
  mat4.identity(mvMatrix);

  // Now move the drawing position a bit to where we want to start
  // drawing the world.

//Drawing P1: Gesla & Virusi / Racunalniska varnost //------------------------------------------------------------------------------------------------------------------

  mat4.translate(mvMatrix, [-2.8, 2.8, -10]);

  mvPushMatrix;
  //mat4.rotate(mvMatrix, degToRad(180), [1, 0, 1]);
  mat4.rotate(mvMatrix, degToRad(rotationP1), [rotateXZ, 0, rotateXZ]);

  // Activate textures
  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, p1Texture);
  gl.uniform1i(shaderProgram.samplerUniform, 0);

  // Set the vertex positions attribute for the teapot vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p1VertexPositionBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, p1VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the texture coordinates attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p1VertexTextureCoordBuffer);
  gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, p1VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the normals attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p1VertexNormalBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, p1VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the index for the vertices.
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p1VertexIndexBuffer);
  setMatrixUniforms();

  // Draw the teapot
  gl.drawElements(gl.TRIANGLES, p1VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

  mvPopMatrix;
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  mat4.identity(mvMatrix);

//Drawing P2: Okrajsave / Zgoscevalne funkcije //-----------------------------------------------------------------------------------------------------------------------

  mat4.translate(mvMatrix, [0, 2.8, -10]);

  mvPushMatrix;
  //mat4.rotate(mvMatrix, degToRad(180), [1, 0, 1]);
  mat4.rotate(mvMatrix, degToRad(rotationP2), [rotateXZ, 0, rotateXZ]);

  // Activate textures
  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, p2Texture);
  gl.uniform1i(shaderProgram.samplerUniform, 0);

  // Set the vertex positions attribute for the teapot vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p2VertexPositionBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, p2VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the texture coordinates attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p2VertexTextureCoordBuffer);
  gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, p2VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the normals attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p2VertexNormalBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, p2VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the index for the vertices.
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p2VertexIndexBuffer);
  setMatrixUniforms();

  // Draw the teapot
  gl.drawElements(gl.TRIANGLES, p2VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

  mvPopMatrix;
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  mat4.identity(mvMatrix);

//Drawing P3: Kode / Teorija kodiranja //----------------------------------------------------------------------------------------------------------------------------------

  mat4.translate(mvMatrix, [2.8, 2.8, -10]);

  mvPushMatrix;
  //mat4.rotate(mvMatrix, degToRad(180), [1, 0, 1]);
  mat4.rotate(mvMatrix, degToRad(rotationP3), [rotateXZ, 0, rotateXZ]);

  // Activate textures
  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, p3Texture);
  gl.uniform1i(shaderProgram.samplerUniform, 0);

  // Set the vertex positions attribute for the teapot vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p3VertexPositionBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, p3VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the texture coordinates attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p3VertexTextureCoordBuffer);
  gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, p3VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the normals attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p3VertexNormalBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, p3VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the index for the vertices.
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p3VertexIndexBuffer);
  setMatrixUniforms();

  // Draw the teapot
  gl.drawElements(gl.TRIANGLES, p3VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

  mvPopMatrix;
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

  mat4.identity(mvMatrix);

// Drawing P4: Sifre / Simetricni kriptosistemi //----------------------------------------------------------------------------------------------------------------  
  mat4.translate(mvMatrix, [-2.8, 0, -10]);

  mvPushMatrix;
  //mat4.rotate(mvMatrix, degToRad(180), [1, 0, 1]);
  mat4.rotate(mvMatrix, degToRad(rotationP4), [rotateXZ, 0, rotateXZ]);

  // Activate textures
  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, p4Texture);
  gl.uniform1i(shaderProgram.samplerUniform, 0);

  // Set the vertex positions attribute for the teapot vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p4VertexPositionBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, p4VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the texture coordinates attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p4VertexTextureCoordBuffer);
  gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, p4VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the normals attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p4VertexNormalBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, p4VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the index for the vertices.
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p4VertexIndexBuffer);
  setMatrixUniforms();

  // Draw the teapot
  gl.drawElements(gl.TRIANGLES, p4VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

  mvPopMatrix;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------

  mat4.identity(mvMatrix);

// Drawing P5: Racunala / Aritmetika & Algoritmi //---------------------------------------------------------------------------------------------  
  mat4.translate(mvMatrix, [0, 0, -10]);

  mvPushMatrix;
  //mat4.rotate(mvMatrix, degToRad(180), [1, 0, 1]);
  mat4.rotate(mvMatrix, degToRad(rotationP5), [rotateXZ, 0, rotateXZ]);

  // Activate textures
  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, p5Texture);
  gl.uniform1i(shaderProgram.samplerUniform, 0);

  // Set the vertex positions attribute for the teapot vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p5VertexPositionBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, p5VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the texture coordinates attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p5VertexTextureCoordBuffer);
  gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, p5VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the normals attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p5VertexNormalBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, p5VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the index for the vertices.
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p5VertexIndexBuffer);
  setMatrixUniforms();

  // Draw the teapot
  gl.drawElements(gl.TRIANGLES, p5VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

  mvPopMatrix;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------

  mat4.identity(mvMatrix);

// Drawing P6: Podpisi / Asimetricni kriptosistemi //-------------------------------------------------------------------------------------------------------------------  
  mat4.translate(mvMatrix, [2.8, 0, -10]);

  mvPushMatrix;
  //mat4.rotate(mvMatrix, degToRad(180), [1, 0, 1]);
  mat4.rotate(mvMatrix, degToRad(rotationP6), [rotateXZ, 0, rotateXZ]);

  // Activate textures
  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, p6Texture);
  gl.uniform1i(shaderProgram.samplerUniform, 0);

  // Set the vertex positions attribute for the teapot vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p6VertexPositionBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, p6VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the texture coordinates attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p6VertexTextureCoordBuffer);
  gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, p6VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the normals attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p6VertexNormalBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, p6VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the index for the vertices.
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p6VertexIndexBuffer);
  setMatrixUniforms();

  // Draw the teapot
  gl.drawElements(gl.TRIANGLES, p6VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

  mvPopMatrix;

//------------------------------------------------------------------------------------------------------------------------------------------------------------------

  mat4.identity(mvMatrix);

// Drawing P7: Protokoli / Kriptografske sheme //-------------------------------------------------------------------------------------------------------------------  
mat4.translate(mvMatrix, [-2.8, -2.8, -10]);

mvPushMatrix;
//mat4.rotate(mvMatrix, degToRad(180), [1, 0, 1]);
mat4.rotate(mvMatrix, degToRad(rotationP7), [rotateXZ, 0, rotateXZ]);

// Activate textures
gl.activeTexture(gl.TEXTURE0);
gl.bindTexture(gl.TEXTURE_2D, p7Texture);
gl.uniform1i(shaderProgram.samplerUniform, 0);

// Set the vertex positions attribute for the teapot vertices.
gl.bindBuffer(gl.ARRAY_BUFFER, p7VertexPositionBuffer);
gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, p7VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

// Set the texture coordinates attribute for the vertices.
gl.bindBuffer(gl.ARRAY_BUFFER, p7VertexTextureCoordBuffer);
gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, p7VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

// Set the normals attribute for the vertices.
gl.bindBuffer(gl.ARRAY_BUFFER, p7VertexNormalBuffer);
gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, p7VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

// Set the index for the vertices.
gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p7VertexIndexBuffer);
setMatrixUniforms();

// Draw the teapot
gl.drawElements(gl.TRIANGLES, p7VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

mvPopMatrix;

//------------------------------------------------------------------------------------------------------------------------------------------------------------------

  mat4.identity(mvMatrix);

// Drawing P8: Loto / Generator nakljucnih stevil //-------------------------------------------------------------------------------------------------------------------  
  mat4.translate(mvMatrix, [0, -2.8, -10]);

  mvPushMatrix;
  //mat4.rotate(mvMatrix, degToRad(180), [1, 0, 1]);
  mat4.rotate(mvMatrix, degToRad(rotationP8), [rotateXZ, 0, rotateXZ]);

  // Activate textures
  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, p8Texture);
  gl.uniform1i(shaderProgram.samplerUniform, 0);

  // Set the vertex positions attribute for the teapot vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p8VertexPositionBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, p8VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the texture coordinates attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p8VertexTextureCoordBuffer);
  gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, p8VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the normals attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p8VertexNormalBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, p8VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the index for the vertices.
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p8VertexIndexBuffer);
  setMatrixUniforms();

  // Draw the teapot
  gl.drawElements(gl.TRIANGLES, p8VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

  mvPopMatrix;

//------------------------------------------------------------------------------------------------------------------------------------------------------------------

  mat4.identity(mvMatrix);

// Drawing P9: Verige / Blocne Verige //-------------------------------------------------------------------------------------------------------------------  
  mat4.translate(mvMatrix, [2.8, -2.8, -10]);

  mvPushMatrix;
  //mat4.rotate(mvMatrix, degToRad(180), [1, 0, 1]);
  mat4.rotate(mvMatrix, degToRad(rotationP9), [rotateXZ, 0, rotateXZ]);

  // Activate textures
  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, p9Texture);
  gl.uniform1i(shaderProgram.samplerUniform, 0);

  // Set the vertex positions attribute for the teapot vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p9VertexPositionBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, p9VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the texture coordinates attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p9VertexTextureCoordBuffer);
  gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, p9VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the normals attribute for the vertices.
  gl.bindBuffer(gl.ARRAY_BUFFER, p9VertexNormalBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, p9VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Set the index for the vertices.
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, p9VertexIndexBuffer);
  setMatrixUniforms();

  // Draw the teapot
  gl.drawElements(gl.TRIANGLES, p9VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

  mvPopMatrix;

//------------------------------------------------------------------------------------------------------------------------------------------------------------------
}

//
// animate
//
// Called every time before redeawing the screen.
//
function animate() {
  var timeNow = new Date().getTime();
  if (lastTime != 0) {
    var elapsed = timeNow - lastTime;

  //P1: Gesla & Verige / Racunalniska varnost
    if (rotateP1 == 0) {
      if (stanjeP1 == 0) {
        if (rotationP1 < 178.0) {
          rotateXZ = 0.25;
          rotationP1 += (90 * elapsed) / 1000.0;
        } else {
          stanjeP1 = 1;
          rotateP1 = 1;
        }
      }
      
      if (stanjeP1 == 1) {
        if (rotationP1 < 358.0) {
          rotateXZ = -0.25;
          rotationP1 += (90 * elapsed) / 1000.0;
        } else {
          rotationP1 = 0.0;
          stanjeP1 = 0;
          rotateP1 = 1;
        }
      }
    }
  //---------------------------------------------
    
  //P2: Okrajsave / Zgoscevalne funkcije
    if (rotateP2 == 0) {
      if (stanjeP2 == 0) {
        if (rotationP2 < 178.0) {
          rotateXZ = 0.25;
          rotationP2 += (90 * elapsed) / 1000.0;
        } else {
          stanjeP2 = 1;
          rotateP2 = 1;
        }
      }
      
      if (stanjeP2 == 1) {
        if (rotationP2 < 358.0) {
          rotateXZ = -0.25;
          rotationP2 += (90 * elapsed) / 1000.0;
        } else {
          rotationP2 = 0.0;
          stanjeP2 = 0;
          rotateP2 = 1;
        }
      }
    }
  //--------------------------------------------------------
    
  //P3: Kode / Teorija kodiranja
    if (rotateP3 == 0) {
      if (stanjeP3 == 0) {
        if (rotationP3 < 178.0) {
          rotateXZ = 0.25;
          rotationP3 += (90 * elapsed) / 1000.0;
        } else {
          stanjeP3 = 1;
          rotateP3 = 1;
        }
      }
      
      if (stanjeP3 == 1) {
        if (rotationP3 < 358.0) {
          rotateXZ = -0.25;
          rotationP3 += (90 * elapsed) / 1000.0;
        } else {
          rotationP3 = 0.0;
          stanjeP3 = 0;
          rotateP3 = 1;
        }
      }
    }
  //-------------------------------------------------------
    
  //P4: Sifre / Simetricni kriptosistemi
    if (rotateP4 == 0) {
      if (stanjeP4 == 0) {
        if (rotationP4 < 178.0) {
          rotateXZ = 0.25;
          rotationP4 += (90 * elapsed) / 1000.0;
        } else {
          stanjeP4 = 1;
          rotateP4 = 1;
        }
        
      }
      
      if (stanjeP4 == 1) {
        if (rotationP4 < 358.0) {
          rotateXZ = -0.25;
          rotationP4 += (90 * elapsed) / 1000.0;
        } else {
          rotationP4 = 0.0;
          stanjeP4 = 0;
          rotateP4 = 1;
        }
        
      }
    }
  //------------------------------------------------------------------
    
  //P5: Racunala / Aritmetika & Algoritmi
    if (rotateP5 == 0) {
      if (stanjeP5 == 0) {
        if (rotationP5 < 178.0) {
          rotateXZ = 0.25;
          rotationP5 += (90 * elapsed) / 1000.0;
        } else {
          stanjeP5 = 1;
          rotateP5 = 1;
        }
      }
      
      if (stanjeP5 == 1) {
        if (rotationP5 < 358.0) {
          rotateXZ = -0.25;
          rotationP5 += (90 * elapsed) / 1000.0;
        } else {
          rotationP5 = 0.0;
          stanjeP5 = 0;
          rotateP5 = 1;
        }
      }
    }
  //----------------------------------------------------------------------
    
  //P6: Podpisi / Asimetricni kriptosistemi
    if (rotateP6 == 0) {
      if (stanjeP6 == 0) {
        if (rotationP6 < 178.0) {
          rotateXZ = 0.25;
          rotationP6 += (90 * elapsed) / 1000.0;
        } else {
          stanjeP6 = 1;
          rotateP6 = 1;
        }
      }
      
      if (stanjeP6 == 1) {
        if (rotationP6 < 358.0) {
          rotateXZ = -0.25;
          rotationP6 += (90 * elapsed) / 1000.0;
        } else {
          rotationP6 = 0.0;
          stanjeP6 = 0;
          rotateP6 = 1;
        }
      }
    }
  //------------------------------------------------------------------------
    
  //P7: Protokoli / Kriptografske sheme
    if (rotateP7 == 0) {
      if (stanjeP7 == 0) {
        if (rotationP7 < 178.0) {
          rotateXZ = 0.25;
          rotationP7 += (90 * elapsed) / 1000.0;
        } else {
          stanjeP7 = 1;
          rotateP7 = 1;
        }
      }
      
      if (stanjeP7 == 1) {
        if (rotationP7 < 358.0) {
          rotateXZ = -0.25;
          rotationP7 += (90 * elapsed) / 1000.0;
        } else {
          rotationP7 = 0.0;
          stanjeP7 = 0;
          rotateP7 = 1;
        }
      }
    }
  //-------------------------------------------------
    
  //P8: Loto / Generator nakljucnih stevil
    if (rotateP8 == 0) {
      if (stanjeP8 == 0) {
        if (rotationP8 < 178.0) {
          rotateXZ = 0.25;
          rotationP8 += (90 * elapsed) / 1000.0;
        } else {
          stanjeP8 = 1;
          rotateP8 = 1;
        }
      }
      
      if (stanjeP8 == 1) {
        if (rotationP8 < 358.0) {
          rotateXZ = -0.25;
          rotationP8 += (90 * elapsed) / 1000.0;
        } else {
          rotationP8 = 0.0;
          stanjeP8 = 0;
          rotateP8 = 1;
        }
      }
    }
  //-----------------------------------------------------------
    
  //P9: Verige / Blocne verige
    if (rotateP9 == 0) {
      if (stanjeP9 == 0) {
        if (rotationP9 < 178.0) {
          rotateXZ = 0.25;
          rotationP9 += (90 * elapsed) / 1000.0;
        } else {
          stanjeP9 = 1;
          rotateP9 = 1;
        }
      }
      
      if (stanjeP9 == 1) {
        if (rotationP9 < 358.0) {
          rotateXZ = -0.25;
          rotationP9 += (90 * elapsed) / 1000.0;
        } else {
          rotationP9 = 0.0;
          stanjeP9 = 0;
          rotateP9 = 1;
        }
      }
    }
  //-----------------------------------------------------------
  
  }
  lastTime = timeNow;
}

function singleVdouble(event) {
  myTimeout = setTimeout(singleClick, 200, event);
}

//
// Mouse handling helper functions
//
// handleMouseDown    ... on mouse button down event
// handleMouseUp      ... on mouse button up event
//
function singleClick(event) {

  ndcX = (event.offsetX / canvas.clientWidth) * 2 - 1;
  ndcY = (1 - (event.offsetY / canvas.clientHeight)) * 2 - 1;

  //To rotate cube P1: Gesla & Virusi / Racunalniska varnost
  if (ndcX > colOneX_left && ndcX < colOneX_right 
    && ndcY < lineOneY_up && ndcY > lineOneY_down) {
    rotateP1 = 0; 
  }

  //To rotate cube P2: Okrajsave / Zgoscevalne funkcije
  if (ndcX > colTwoX_left && ndcX < colTwoX_right 
    && ndcY < lineOneY_up && ndcY > lineOneY_down) {
    rotateP2 = 0; 
  }

  //To rotate cube P3: Kode / Teorija kodiranja
  if (ndcX > colThreeX_left && ndcX < colThreeX_right 
    && ndcY < lineOneY_up && ndcY > lineOneY_down) {
    rotateP3 = 0;
  }

  //To rotate cube P4: Sifre / Simetricni kriptosistemi
  if (ndcX > colOneX_left && ndcX < colOneX_right 
    && ndcY < lineTwoY_up && ndcY > lineTwoY_down) {
    rotateP4 = 0;
  }

  //To rotate cube P5: Racunala / Aritmetika & Algoritmi
  if (ndcX > colTwoX_left && ndcX < colTwoX_right 
    && ndcY < lineTwoY_up && ndcY > lineTwoY_down) {
    rotateP5 = 0; //
  }

  //To rotate cube P6: Podpisi / Asimetricni kriptosistemi
  if (ndcX > colThreeX_left && ndcX < colThreeX_right 
    && ndcY < lineTwoY_up && ndcY > lineTwoY_down) {
    rotateP6 = 0; //
  }

  //To rotate cube P7: Protokoli / Kriptografske sheme
  if (ndcX > colOneX_left && ndcX < colOneX_right 
    && ndcY < lineThreeY_up && ndcY > lineThreeY_down) {
    rotateP7 = 0; //
  }

  //To rotate cube P8: Loto / Generator nakljucnih stevil
  if (ndcX > colTwoX_left && ndcX < colTwoX_right 
    && ndcY < lineThreeY_up && ndcY > lineThreeY_down) {
    rotateP8 = 0; //
  }

  //To rotate cube P9: Verige / Blocne verige
  if (ndcX > colThreeX_left && ndcX < colThreeX_right 
    && ndcY < lineThreeY_up && ndcY > lineThreeY_down) {
    rotateP9 = 0; //
  }

}

function doubleClick(event) {

  clearTimeout(myTimeout);

  ndcX = (event.offsetX / canvas.clientWidth) * 2 - 1;
  ndcY = (1 - (event.offsetY / canvas.clientHeight)) * 2 - 1;

  //To rotate cube P1: Gesla & Virusi / Racunalniska varnost
  if (ndcX > colOneX_left && ndcX < colOneX_right 
    && ndcY < lineOneY_up && ndcY > lineOneY_down) {
    window.location.href = linkP1;
  }

  //To rotate cube P2: Okrajsave / Zgoscevalne funkcije
  if (ndcX > colTwoX_left && ndcX < colTwoX_right 
    && ndcY < lineOneY_up && ndcY > lineOneY_down) {
    window.location.href = linkP2;
  }

  //To rotate cube P3: Kode / Teorija kodiranja
  if (ndcX > colThreeX_left && ndcX < colThreeX_right 
    && ndcY < lineOneY_up && ndcY > lineOneY_down) {
    window.location.href = linkP3;
  }

  //To rotate cube P4: Sifre / Simetricni kriptosistemi
  if (ndcX > colOneX_left && ndcX < colOneX_right 
    && ndcY < lineTwoY_up && ndcY > lineTwoY_down) {
    window.location.href = linkP4;
  }

  //To rotate cube P5: Racunala / Aritmetika & Algoritmi
  if (ndcX > colTwoX_left && ndcX < colTwoX_right 
    && ndcY < lineTwoY_up && ndcY > lineTwoY_down) {
    window.location.href = linkP5;
  }

  //To rotate cube P6: Podpisi / Asimetricni kriptosistemi
  if (ndcX > colThreeX_left && ndcX < colThreeX_right 
    && ndcY < lineTwoY_up && ndcY > lineTwoY_down) {
    window.location.href = linkP6;
  }

  //To rotate cube P7: Protokoli / Kriptografske sheme
  if (ndcX > colOneX_left && ndcX < colOneX_right 
    && ndcY < lineThreeY_up && ndcY > lineThreeY_down) {
    window.location.href = linkP7;
  }

  //To rotate cube P8: Loto / Generator nakljucnih stevil
  if (ndcX > colTwoX_left && ndcX < colTwoX_right 
    && ndcY < lineThreeY_up && ndcY > lineThreeY_down) {
    window.location.href = linkP8;
  }

  //To rotate cube P9: Verige / Blocne verige
  if (ndcX > colThreeX_left && ndcX < colThreeX_right 
    && ndcY < lineThreeY_up && ndcY > lineThreeY_down) {
    window.location.href = linkP9;
  }
}

function resize(canvas) {
  // Lookup the size the browser is displaying the canvas.
  var displayWidth = canvas.clientWidth;
  var displayHeight = canvas.clientHeight;

  // Check if the canvas is not the same size.
  if (canvas.width != displayWidth || canvas.height != displayHeight) {

    // Make the canvas the same size
    canvas.width = displayWidth;
    canvas.height = displayHeight;
  }
}

//
// start
//
// Called when the canvas is created to get the ball rolling.
// Figuratively, that is. There's nothing moving in this demo.
//
function start() {
  canvas = document.getElementById("glcanvas");

  gl = initGL(canvas);      // Initialize the GL context
  if (gl) {
    gl.clearColor(0.0, 0.0, 0.0, 1.0);                      // Set clear color to black, fully opaque
    gl.clearDepth(1.0);                                     // Clear everything
    gl.enable(gl.DEPTH_TEST);                               // Enable depth testing
    gl.depthFunc(gl.LEQUAL);                                // Near things obscure far things

    // Initialize the shaders; this is where all the lighting for the
    // vertices and so forth is established.
    initShaders();
    
    // Next, load and set up the textures we'll be using.
    initTextures();
    loadCube();
    initBuffers();

    // Bind mouse handling functions to document handlers
    canvas.onclick = singleVdouble;
    canvas.ondblclick = doubleClick;
    
    // Set up to draw the scene periodically.
    setInterval(function() {
      if (texturesLoaded == numberOfTextures) { // only draw scene and animate when textures are loaded.
        requestAnimationFrame(animate);
        drawScene();
      }
    }, 15);
  }
}